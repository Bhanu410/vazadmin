//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace sb_admin_2.Web.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Customers
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ComapanyName { get; set; }
        public Nullable<long> LocationId { get; set; }
        public Nullable<long> SubLocationId { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string CustomerId { get; set; }
        public Nullable<int> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> LastUpdatedAt { get; set; }
        public string ImagePath { get; set; }
    }
}
