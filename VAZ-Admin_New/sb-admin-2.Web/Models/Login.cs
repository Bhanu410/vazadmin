﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Models
{
    public class Login
    {
        public long Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public string Role { get; set; }
    }

    public class ChangePassword
    {
        [Required(ErrorMessage = "Please enter password")]
        [System.Web.Mvc.Compare("ConfirmPassword", ErrorMessage = "Password doesn't match.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter confirm password")]
        public string ConfirmPassword { get; set; }
    }
}