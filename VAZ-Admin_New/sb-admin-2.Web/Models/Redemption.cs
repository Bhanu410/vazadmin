﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Models
{
  public class Redemption
  {
    public long Id { get; set; }
    public Nullable<long> UserId { get; set; }
    public string Points { get; set; }
    public string Amount { get; set; }
    public string AccountHolderName { get; set; }
    public string AccountNumber { get; set; }
    public string BankName { get; set; }
    public string IFSC { get; set; }
    public string Mobile { get; set; }
    public Nullable<bool> IsPending { get; set; }
    public Nullable<System.DateTime> CreatedAt { get; set; }
    public string strCreatedAt { get; set; }
    public Nullable<System.DateTime> ApprovedAt { get; set; }
    public string strApprovedAt { get; set; }
    public string Status { get; set; }
    public string UUId { get; set; }
  }

  public class PointsDefinition
  {
    [Required(ErrorMessage = "Please enter Points.")]
    public Nullable<int> Points { get; set; }

    [Required(ErrorMessage = "Please enter Rupees.")]
    public Nullable<int> Rupees { get; set; }
  }
}