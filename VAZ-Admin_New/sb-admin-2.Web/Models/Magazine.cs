﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public class Magazine : BaseModel
  {
    public long Id { get; set; }

    [Required(ErrorMessage = "Please enter title")]
    public string Title { get; set; }
    public string Description { get; set; }

    [Required(ErrorMessage = "Please select the location")]
    public Nullable<long> LocationId { get; set; }

    
    public Nullable<System.DateTime> ExpiryDate { get; set; }
    public Nullable<bool> IsActive { get; set; }
    public Nullable<System.DateTime> CreatedAt { get; set; }
    public Nullable<long> CreatedBy { get; set; }
    public Nullable<System.DateTime> LastUpdatedAt { get; set; }
    public Nullable<long> LastUpdatedBy { get; set; }
    public string Location { get; internal set; }
    public string Status { get; internal set; }

    [Required(ErrorMessage = "Please select the expiry date")]
    public string strExpiryDate { get; set; }

    [Required(ErrorMessage = "Please select the magazine date")]
    public string strMagazineDate { get; set; }
    public string strCreatedAt { get; set; }

    public string ImagePath { get; set; }

    //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
    public HttpPostedFileBase file { get; set; }
    public Nullable<System.DateTime> MagazineDate { get; set; }

    public List<MagazineImages> MagazineImages { get; set; }
    public string MagazineLocation { get;  set; }
    public string MagazineSubLocation { get; set; }
  }

  public class MagazineImages
  {
    public long Id { get; set; }
    public Nullable<long> MagazineId { get; set; }
    public string ImagePath { get; set; }
  }

  public class MagazineVM
  {
    public Magazine Magazine { get; set; }
    public List<SelectListItem> LocationList { get; set; }

  }
}