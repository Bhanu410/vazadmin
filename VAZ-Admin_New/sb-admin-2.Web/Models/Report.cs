﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public class Report
  {
    public string CustomerName { get; set; }
    public string CustomerId { get; set; }
    public ReportFilter ReportFilter { get; set; }
    public List<SelectListItem> LocationList { get; set; }
    public List<Advertisement> Adds { get; set; }
    public List<Video> Videos { get; set; }
    public List<Banner> Banners { get; set; }
    public List<User> Users { get; set; }
    public List<Customer> Customers { get; set; }
    public List<Store> Stores { get; set; }
    public List<Offers> Offers { get; set; }
  }

  public static class ReportTypes
  {
    public const string ADD = "ADD", VIDEO = "VIDEO", BANNER = "BANNER", USER = "USER", STORE = "STORE", CUSTOMER = "CUSTOMER", CUSTOMERACTIVITY = "CUSTOMERACTIVITY";
  }


  public class ReportFilter
  {
    // [Required(ErrorMessage = "Please enter offer start date.")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
    public Nullable<System.DateTime> FromDate { get; set; }

    //[Required(ErrorMessage = "Please enter offer start date.")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
    public Nullable<System.DateTime> ToDate { get; set; }

    public string strFromDate { get; set; }
    public string strToDate { get; set; }

    public string Status { get; set; }

    public long LocationId { get; set; }

    public string CustomerId { get; set; }

    public string Type { get; set; }
  }

  public class RptAdvertisement
  {
    public string Title { get; set; }
    public string Description { get; set; }
    public int? Time { get; set; }
    public string Location { get; set; }
    public string DateofUpload { get; set; }
    public int? ViewsLeft { get; set; }
    public int? RewardPoints { get; set; }
    
    public string Customer { get; set; }
    public string CustomerId { get; set; }
    public string Status { get; set; }
  }

  public class BannerRpt
  {
    public string Location { get; set; }
    public string DateofUpload { get; set; }

    //public string Store { get; set; }
    public string Customer { get; set; }
    public string CustomerId { get; set; }
    public string Status { get; set; }

    public string ExpiryDate { get; set; }
  }

  public class UserRpt
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Mobile { get; set; }
    public string Email { get; set; }
    public string Location { get; set; }
    public string Subscription { get; set; }
    public string Status { get; set; }
    public string RegisteredOn { get; set; }
    public string UID { get; set; }
  }

  public class CustomerRpt
  {
    public string Name { get; set; }
    public string CompanyName { get; set; }
    public string Mobile { get; set; }
    public string Email { get; set; }
    public string CustomerId { get; set; }
    public string Location { get; set; }
    public string SubLocation { get; set; }
    public DateTime? RegisteredOn { get; set; }
  }

  public class StoreRpt
  {
    public string Name { get; set; }
    public string CustomerId { get; set; }
    public string Location { get; set; }
    public string SubLocation { get; set; }
  }

  public class CustomerActivityReport
  {
    public string Type { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Location { get; set; }
    public string DateofUpload { get; set; }
    public int ViewsLeft { get; set; }
    public string ExpiryDate { get; set; }
    public int RewardPoints { get; set; }
    public string Store { get; set; }
    public string Customer { get; set; }
    public string CustomerId { get; set; }
    public string Status { get; set; }

  }
}