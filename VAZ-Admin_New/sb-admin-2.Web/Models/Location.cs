﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public partial class Location : BaseModel
  {
    public long Id { get; set; }

    [Required(ErrorMessage = "Please enter location.")]
    public string Name { get; set; }
    public int IsActive { get; set; }

    public bool Flag { get; set; }
  }

  public partial class SubLocation : BaseModel
  {
    public List<SelectListItem> LocationList { get; set; }

    public long Id { get; set; }

    [Required(ErrorMessage = "Please select location.")]
    public long LocationId { get; set; }
    public string LocationName { get; set; }

    [Required(ErrorMessage = "Please enter sub location.")]
    public string Name { get; set; }
    public int IsActive { get; set; }

    public bool Flag { get; set; }
  }
}