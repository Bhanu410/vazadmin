﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public class NewsDTO : BaseModel
  {
    public long Id { get; set; }

    [Required(ErrorMessage = "Please enter title")]
    public string Title { get; set; }

    [Required(ErrorMessage = "Please add NEWS")]
    public string News { get; set; }
    public bool IsHeadline { get; set; }

    
    public Nullable<System.DateTime> ExpiryDate { get; set; }

    [Required(ErrorMessage = "Please select the location")]
    public Nullable<long> LocationId { get; set; }

    [Required(ErrorMessage = "Please select the sub location")]
    public Nullable<long> SubLocationId { get; set; }
    public Nullable<bool> IsActive { get; set; }
    public Nullable<System.DateTime> CreatedAt { get; set; }
    public Nullable<long> CreatedBy { get; set; }
    public Nullable<System.DateTime> LastUpdatedAt { get; set; }
    public Nullable<long> LastUpdatedBy { get; set; }

    public string Status { get; set; }
    public string Location { get; set; }
    public string Sublocation { get; set; }

    [Required(ErrorMessage = "Please select expiry date")]
    public string strExpiryDate { get; set; }

    //[Required(ErrorMessage = "Please select created date")]
    public string strCreatedAt { get; set; }

    [Required(ErrorMessage = "Please select category")]
    public Nullable<long> CategoryId { get; set; }
    public List<NewsImages> NewsImages { get;  set; }
    public string Category { get; internal set; }

    public string NewsSublocation { get; set; }
    public string NewsLocation { get; set; }

    public bool IsClassified { get; set; }
  }

  public partial class NewsImages
  {
    public long Id { get; set; }
    public Nullable<long> NewsId { get; set; }
    public string ImagePath { get; set; }
  }

  public class NewsVM
  {
    public NewsDTO NewsObj { get; set; }
    public List<SelectListItem> LocationList { get; set; }
    public List<SelectListItem> SubLocationList { get; set; }
    public List<SelectListItem> CategoryList { get; internal set; }
  }

}