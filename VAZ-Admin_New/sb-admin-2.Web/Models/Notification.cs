﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public class Notification
  {
    public List<SelectListItem> UsersList { get; set; }

    public long ID { get; set; }

    [Required(ErrorMessage = "Please select user")]
    public long UserId { get; set; }

    [Required(ErrorMessage = "Please enter title")]
    public string Title { get; set; }

    [Required(ErrorMessage = "Please enter message")]
    [StringLength(500, ErrorMessage = "Message cannot be longer than 500 characters.")]
    public string Message { get; set; }

    public string For { get; set; }

    public string SentTo { get; set; }
    public string Mobile { get; set; }

    public Nullable<System.DateTime> CreatedAt { get; set; }

    public string ImagePath { get; set; }

    public string FCMUsers { get; set; }
  }

  public class NotificationOutbox
  {
    public long UserId { get; set; }
    public string Title { get; set; }
    public string Message { get; set; }
    public string SentTo { get; set; }
    public string Mobile { get; set; }
    public Nullable<System.DateTime> CreatedAt { get; set; }
  }

}