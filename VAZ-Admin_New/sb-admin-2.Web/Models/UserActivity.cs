﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Models
{
    public class Activity
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public int RewardPoints { get; set; }
        public string ViewedDate { get; set; }
    }

    public class ActivityVM 
    {
        public int? TotalPoints { get; set; }
        public int? AddPoints { get; set; }
        public int? VideoPoints { get; set; }
        public List<Activity> TransactionList { get; set; }
        public List<Redemption> Redemptions { get; set; }
    }
}