﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
    public class OfferVM
    {
        public OfferSubCategories SubCategory { get; set; }
        public Offers Offer { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> SubCategoryList { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public List<SelectListItem> SubLocationList { get; set; }
        public List<SelectListItem> StoreList { get; set; }
    }

    public class OfferCategories : BaseModel
  {
        public long Id { get; set; }
        public string UniqueName { get; set; }
        [Required (ErrorMessage = "Enter category.")]
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public Nullable<int> IsActive { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase file { get; set; }
    }

    public class OfferSubCategories
    {
        public long Id { get; set; }

        [Required(ErrorMessage= "Please select category.")]
        public Nullable<long> MasterId { get; set; }
        public string UniqueName { get; set; }

        [Required(ErrorMessage = "Please enter sub category name.")]
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public Nullable<int> IsActive { get; set; }

       
        public Nullable<long> LocationId { get; set; }
        public string LocationName { get; set; }

     
        public Nullable<long> SubLocationId { get; set; }
        public string SubLocationName { get; set; }

        public string Category { get; set; }

      
        public string Address { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase file { get; set; }
    }

    public class Offers
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please select category.")]
        public Nullable<long> CategoryId { get; set; }

        [Required(ErrorMessage = "Please select store name.")]
        public Nullable<long> SubCategoryId { get; set; }

        [Required(ErrorMessage = "Please enter product name.")]
        public string ProductName { get; set; }

        public string ImagePath { get; set; }

        
        public Nullable<int> OfferPercent { get; set; }

        [Required(ErrorMessage = "Please enter offer start date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> OfferStartDate { get; set; }

         [Required(ErrorMessage = "Please enter offer end date.")]
         [DataType(DataType.Date)]
         [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> OfferEndDate { get; set; }

        [Required(ErrorMessage = "Please enter description.")]
        public string Description { get; set; }
     
        public Nullable<int> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdatedAt { get; set; }
        public Nullable<System.DateTime> LastUpdatedBy { get; set; }

        public string Category { get; set; }
        public string SubCategory { get; set; }

        public string ImageTwo { get; set; }
        public string ImageThree { get; set; }
        public string ImageFour { get; set; }
        public string ImageFive { get; set; }
        public string ImageSix { get; set; }


        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase file { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase fileTwo { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase fileThree { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase fileFour { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase fileFive { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase fileSix { get; set; }

        public string Status { get; set; }

        [Required(ErrorMessage = "Please enter offer start date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public string strOfferStartDate { get; set; }

        [Required(ErrorMessage = "Please enter offer end date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public string strOfferEndDate { get; set; }

        [Required(ErrorMessage = "Please select location.")]
        public Nullable<long> LocationId { get; set; }

        [Required(ErrorMessage = "Please select sub location.")]
        public Nullable<long> SubLocationId { get; set; }

        [Required(ErrorMessage = "Please select store.")]
        public Nullable<long> StoreId { get; set; }

        [Required(ErrorMessage = "Please enter store address.")]
        public string Address { get; set; }

        public List<OfferImages> OfferImages { get; set; }

        public string StoreName { get; set; }
        public string CustomerId { get; set; }

    }

    public partial class OfferImages
    {
        public long Id { get; set; }
        public Nullable<long> OfferId { get; set; }
        public string ImagePath { get; set; }
    }
}