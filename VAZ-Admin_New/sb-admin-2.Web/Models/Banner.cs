﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public class Banner : BaseModel
  {
    public long Id { get; set; }
    public string ImagePath { get; set; }
    public Nullable<int> IsActive { get; set; }

    [Required(ErrorMessage = "Please select the location")]
    public Nullable<long> LocationId { get; set; }
    public string LocationName { get; set; }

    public Nullable<System.DateTime> CreatedDate { get; set; }
    public Nullable<System.DateTime> ExpireDate { get; set; }

    [Required(ErrorMessage = "Please enter days.")]
    public double Days { get; set; }
    public string Status { get; set; }

    public string strCreatedDate { get; set; }

    [Required(ErrorMessage = "Please select expiry date")]
    public string strExpiryDate { get; set; }

    // [Required(ErrorMessage = "Please select file.")]
    //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
    public HttpPostedFileBase file { get; set; }

    public Nullable<long> StoreId { get; set; }

    [Required(ErrorMessage = "Please select the sub location")]
    public Nullable<long> SubLocationId { get; set; }

    public string StoreName { get; set; }
    public string CustomerName { get; set; }
    public string CustomerId { get; set; }

    public string BannerLocation { get; set; }
    public string BannerSubLocation { get; set; }

  }

  public class BannerVM
  {
    public List<Banner> BannerList { get; set; }
    public Banner Banner { get; set; }
    public List<SelectListItem> LocationList { get; set; }
    public List<SelectListItem> SubLocationList { get; set; }
    public List<SelectListItem> StoreList { get; set; }
  }


}