﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Models
{
    public class DashBoard
    {
        public int Users { get; set; }
        public int Adds { get; set; }
        public int Videos { get; set; }
        public int Customers { get; set; }

        public string UserDataPoints { get; set; }
        public string AddDataPoints { get; set; }
        public string VideoDataPoints { get; set; }
        public string CustomerDataPoints { get; set; }

    }

    public class Point
    {
        public string x { get; set; }
        public int y { get; set; }
    }
}