﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
    public class Advertisement : BaseModel
    {
        public List<SelectListItem> LocationList { get; set; }
        public List<SelectListItem> SubLocationList { get; set; }

        public List<SelectListItem> StoreList { get; set; }

        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter title.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter description.")]
        public string Description { get; set; }

        public string ImagePath { get; set; }

        [Required(ErrorMessage = "Please enter reward points.")]
        public Nullable<int> RewardPoints { get; set; }

        [Required(ErrorMessage = "Please select location.")]
        public Nullable<long> LocationId { get; set; }

        public string LocationName { get; set; }
        public Nullable<int> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdatedAt { get; set; }
        public Nullable<long> LastUpdatedBy { get; set; }

        [Required(ErrorMessage = "Please enter time in seconds.")]
        public Nullable<int> Time { get; set; }

        [Required(ErrorMessage = "Please enter no of views.")]
        public Nullable<int> ViewsLimit { get; set; }

        //[Required(ErrorMessage = "Please select file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase file { get; set; }

        public string strCreatedAt { get; set; }
        public string Status { get; set; }

        public Nullable<long> StoreId { get; set; }
        public Nullable<long> SubLocationId { get; set; }

        public string StoreName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
    public string AddLocation { get;  set; }
    public string AddSubLocation { get;  set; }
  }



}