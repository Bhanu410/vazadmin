﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
    public class Customer : BaseModel
  {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter name.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter company name.")]
        public string CompanyName { get; set; }

         [Required(ErrorMessage = "Please select location.")]
        public Nullable<long> LocationId { get; set; }

        
        public Nullable<long> SubLocationId { get; set; }

         [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
         [Required(ErrorMessage = "Please enter mobile number")]
        public string Mobile { get; set; }

        public string Email { get; set; }
        public string CustomerId { get; set; }

        public Nullable<int> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> LastUpdatedAt { get; set; }
        public string ImagePath { get; set; }

        public string SubLocationName { get; set; }

        public string LocationName { get; set; }

        public string strCreatedAt { get; set; }
    }

    public class CustomerVM
    {
        public List<SelectListItem> LocationList { get; set; }
        public List<SelectListItem> SubLocationList { get; set; }
        public List<Customer> CustomerList { get; set; }
        public Customer Customer { get; set; }
    }
}