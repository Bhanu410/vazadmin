﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Models
{
  public class BaseModel
  {
    public string PageTitle { get; set; }
    public string ButtonTitle { get; set; }
  }
}