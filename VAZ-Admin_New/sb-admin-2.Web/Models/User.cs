﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
  public class User
  {


    public long Id { get; set; }
    public string UserId { get; set; }

    [Required(ErrorMessage = "Please enter first name")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Please enter last name")]
    public string LastName { get; set; }

    [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
    [Required(ErrorMessage = "Please enter mobile number")]
    public string Mobile { get; set; }


    public string Email { get; set; }

    [Required(ErrorMessage = "Please select location")]
    public Nullable<long> LocationId { get; set; }

    public string Password { get; set; }
    public string PasswordSalt { get; set; }

    public string Address { get; set; }

    public string LocationName { get; set; }
    public Nullable<int> IsActive { get; set; }
    public string SubscriptionType { get; set; }
    public Nullable<int> VerificationCode { get; set; }
    public Nullable<System.DateTime> CreatedAt { get; set; }
    public Nullable<System.DateTime> LastUpdatedAt { get; set; }
    public string IsNotificationEnabled { get; set; }
    public string Gender { get; set; }
    public string Dob { get; set; }
    public string SubscriptionPlan { get; set; }
    public Nullable<System.DateTime> SubscriptionStartDate { get; set; }
    public Nullable<System.DateTime> SubscriptionEndDate { get; set; }
    public string DeviceToken { get; set; }
    public string DeviceType { get; set; }
    public string ProfileImage { get; set; }

    public string strCreatedAt { get; set; }
    public string Status { get; set; }

    public string strUserId { get; set; }
  }

  public class UserVM
  {
    public User User { get; set; }
    public List<SelectListItem> LocationList { get; set; }
  }

}