﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Models
{
    public class Store
    {
        public long Id { get; set; }
        public string UniqueId { get; set; }

        [Required(ErrorMessage = "Please enter store name.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please select location.")]
        public Nullable<long> LocationId { get; set; }

        [Required(ErrorMessage = "Please select sub location.")]
        public Nullable<long> SubLocationId { get; set; }
        public Nullable<int> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> LastUpdatedAt { get; set; }
        public string ImagePath { get; set; }

        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase file { get; set; }
        public string LocationName { get; set; }
        public string SubLocationName { get; set; }

        [Required(ErrorMessage = "Please enter CustomerId.")]
        //[Remote("CheckCustomer", "Customer", ErrorMessage = "Customer does not exist!")]
        public string CustomerId { get; set; }

        

    }

    public class StoreVM
    {
        public List<SelectListItem> LocationList { get; set; }
        public List<SelectListItem> SubLocationList { get; set; }
        public List<Store> StoresList { get; set; }
        public Store Store { get; set; }

        public List<Banner> BannerList { get; set; }
        public List<Advertisement> AddList { get; set; }
        public List<Video> VideoList { get; set; }
        public List<Offers> OfferList { get; set; }
    }
}