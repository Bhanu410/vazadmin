﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Models
{
    public class GlobalValues
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}