﻿using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Domain
{
  public class Data
  {
    public IEnumerable<Navbar> navbarItems()
    {
      var menu = new List<Navbar>();
      menu.Add(new Navbar { Id = 1, nameOption = "Dashboard", controller = "Home", action = "Index", imageClass = "fa fa-dashboard fa-fw", status = true, isParent = false, parentId = 0 });
      //menu.Add(new Navbar { Id = 2, nameOption = "Charts", imageClass = "fa fa-bar-chart-o fa-fw", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 3, nameOption = "Flot Charts", controller = "Home", action = "FlotCharts", status = true, isParent = false, parentId = 2 });
      //menu.Add(new Navbar { Id = 4, nameOption = "Morris.js Charts", controller = "Home", action = "MorrisCharts", status = true, isParent = false, parentId = 2 });
      //menu.Add(new Navbar { Id = 5, nameOption = "Tables", controller = "Home", action = "Tables", imageClass = "fa fa-table fa-fw", status = true, isParent = false, parentId = 0 });
      //menu.Add(new Navbar { Id = 6, nameOption = "Forms", controller = "Home", action = "Forms", imageClass = "fa fa-edit fa-fw", status = true, isParent = false, parentId = 0 });
      //menu.Add(new Navbar { Id = 7, nameOption = "UI Elements", imageClass = "fa fa-wrench fa-fw", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 8, nameOption = "Panels and Wells", controller = "Home", action = "Panels", status = true, isParent = false, parentId = 7 });
      //menu.Add(new Navbar { Id = 9, nameOption = "Buttons", controller = "Home", action = "Buttons", status = true, isParent = false, parentId = 7 });
      //menu.Add(new Navbar { Id = 10, nameOption = "Notifications", controller = "Home", action = "Notifications", status = true, isParent = false, parentId = 7 });
      //menu.Add(new Navbar { Id = 11, nameOption = "Typography", controller = "Home", action = "Typography", status = true, isParent = false, parentId = 7 });
      //menu.Add(new Navbar { Id = 12, nameOption = "Icons", controller = "Home", action = "Icons", status = true, isParent = false, parentId = 7 });
      //menu.Add(new Navbar { Id = 13, nameOption = "Grid", controller = "Home", action = "Grid", status = true, isParent = false, parentId = 7 });
      //menu.Add(new Navbar { Id = 14, nameOption = "Multi-Level Dropdown", imageClass = "fa fa-sitemap fa-fw", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 15, nameOption = "Second Level Item", status = true, isParent = false, parentId = 14 });
      //menu.Add(new Navbar { Id = 16, nameOption = "Sample Pages", imageClass = "fa fa-files-o fa-fw", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 17, nameOption = "Blank Page", controller = "Home", action = "Blank", status = true, isParent = false, parentId = 16 });
      //menu.Add(new Navbar { Id = 18, nameOption = "Login Page", controller = "Home", action = "Login", status = true, isParent = false, parentId = 16 });

      menu.Add(new Navbar { Id = 19, nameOption = "Add Management", imageClass = "fa fa-image fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 20, nameOption = "Add", controller = "Advertisement", action = "PostAdd", status = true, isParent = false, parentId = 19 });
      menu.Add(new Navbar { Id = 21, nameOption = "View", controller = "Advertisement", action = "GetAdds", status = true, isParent = false, parentId = 19 });

      menu.Add(new Navbar { Id = 22, nameOption = "Video Management", imageClass = "fa fa-youtube fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 23, nameOption = "Add", controller = "Video", action = "PostVideo", status = true, isParent = false, parentId = 22 });
      menu.Add(new Navbar { Id = 24, nameOption = "View", controller = "Video", action = "GetVideos", status = true, isParent = false, parentId = 22 });

      menu.Add(new Navbar { Id = 25, nameOption = "Categories", imageClass = "fa fa-table fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 26, nameOption = "Add", controller = "Offer", action = "PostCategories", status = true, isParent = false, parentId = 25 });
      menu.Add(new Navbar { Id = 27, nameOption = "View", controller = "Offer", action = "GetCategories", status = true, isParent = false, parentId = 25 });

      //menu.Add(new Navbar { Id = 28, nameOption = "Offer SubCategories", imageClass = "fa fa-sitemap fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 29, nameOption = "Add", controller = "Offer", action = "PostSubCategories", status = true, isParent = false, parentId = 28 });
      //menu.Add(new Navbar { Id = 30, nameOption = "View", controller = "Offer", action = "GetSubCategories", status = true, isParent = false, parentId = 28 });

      //menu.Add(new Navbar { Id = 31, nameOption = "Offer Management", imageClass = "fa fa-tags fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 32, nameOption = "Add", controller = "Offer", action = "PostOffers", status = true, isParent = false, parentId = 31 });
      //menu.Add(new Navbar { Id = 33, nameOption = "View", controller = "Offer", action = "GetOffers", status = true, isParent = false, parentId = 31 });

      menu.Add(new Navbar { Id = 34, nameOption = "Location Management", imageClass = "fa fa-location-arrow fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 35, nameOption = "Add", controller = "Location", action = "PostLocations", status = true, isParent = false, parentId = 34 });
      menu.Add(new Navbar { Id = 36, nameOption = "View", controller = "Location", action = "GetLocations", status = true, isParent = false, parentId = 34 });

      menu.Add(new Navbar { Id = 37, nameOption = "Sub Location Management", imageClass = "fa fa-location-arrow fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 38, nameOption = "Add", controller = "Location", action = "PostSubLocations", status = true, isParent = false, parentId = 37 });
      menu.Add(new Navbar { Id = 39, nameOption = "View", controller = "Location", action = "GetSubLocations", status = true, isParent = false, parentId = 37 });

      menu.Add(new Navbar { Id = 40, nameOption = "Banners Management", imageClass = "fa fa-image fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 41, nameOption = "Add", controller = "Banner", action = "PostBanners", status = true, isParent = false, parentId = 40 });
      menu.Add(new Navbar { Id = 42, nameOption = "View", controller = "Banner", action = "GetBanners", status = true, isParent = false, parentId = 40 });



      menu.Add(new Navbar { Id = 44, nameOption = "App Users", controller = "User", action = "Index", imageClass = "fa fa-user fa-fw", status = true, isParent = false, parentId = 0 });

      //menu.Add(new Navbar { Id = 45, nameOption = "Store Management", imageClass = "fa fa-home fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      //menu.Add(new Navbar { Id = 46, nameOption = "Add", controller = "Store", action = "AddStore", status = true, isParent = false, parentId = 45 });
      //menu.Add(new Navbar { Id = 47, nameOption = "View", controller = "Store", action = "GetStores", status = true, isParent = false, parentId = 45 });

      menu.Add(new Navbar { Id = 48, nameOption = "Customer Management", imageClass = "fa fa-user fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 49, nameOption = "Add", controller = "Customer", action = "AddCustomer", status = true, isParent = false, parentId = 48 });
      menu.Add(new Navbar { Id = 50, nameOption = "View", controller = "Customer", action = "GetCustomers", status = true, isParent = false, parentId = 48 });



      menu.Add(new Navbar { Id = 52, nameOption = "Notification", imageClass = "fa fa-pencil fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 53, nameOption = "New", controller = "Notification", action = "SendNotification", status = true, isParent = false, parentId = 52 });
      menu.Add(new Navbar { Id = 54, nameOption = "Outbox", controller = "Notification", action = "NotificationOutbox", status = true, isParent = false, parentId = 52 });

      menu.Add(new Navbar { Id = 55, nameOption = "News Management", imageClass = "fa fa-newspaper-o  fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 56, nameOption = "Add", controller = "News", action = "PostNews", status = true, isParent = false, parentId = 55 });
      menu.Add(new Navbar { Id = 57, nameOption = "View", controller = "News", action = "GetNews", status = true, isParent = false, parentId = 55 });

      menu.Add(new Navbar { Id = 58, nameOption = "Magazine Management", imageClass = "fa fa-film fa-fw", controller = "Home", action = "Login", status = true, isParent = true, parentId = 0 });
      menu.Add(new Navbar { Id = 59, nameOption = "Add", controller = "Magazine", action = "PostMagazine", status = true, isParent = false, parentId = 58 });
      menu.Add(new Navbar { Id = 60, nameOption = "View", controller = "Magazine", action = "GetMagazine", status = true, isParent = false, parentId = 58 });


      return menu.ToList();
    }
  }
}