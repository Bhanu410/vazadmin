﻿using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Mappers
{
  public class AdvertisementMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    public Advertisement SingleAdd(long Id)
    {
      var data = (from a in _db.tbl_Adds
                  where a.Id == Id
                  select a).ToList();

      return (from a in data
              select new Advertisement
              {
                Id = a.Id,
                Name = a.Name,
                Description = a.Description,
                ImagePath = a.ImagePath,
                RewardPoints = a.RewardPoints,
                LocationId = a.LocationId,
                Time = a.Time,
                ViewsLimit = a.ViewsLimit,
                strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                SubLocationId = a.SubLocationId,
                StoreId = a.StoreId
              }).SingleOrDefault();
    }
    public int PostAdd(Advertisement Obj)
    {
      tbl_Adds tblAdds = new tbl_Adds();
      string uId = Guid.NewGuid().ToString();

      tblAdds.Name = Obj.Name;
      tblAdds.ImagePath = Obj.ImagePath;
      tblAdds.Description = Obj.Description;
      tblAdds.RewardPoints = Obj.RewardPoints;
      tblAdds.LocationId = Obj.LocationId;
      tblAdds.LocationName = Obj.LocationName;
      tblAdds.CreatedAt = CurrentIndianDate();
      tblAdds.IsActive = 1;
      tblAdds.Time = Obj.Time;
      tblAdds.ViewsLimit = Obj.ViewsLimit;
      tblAdds.StoreId = Obj.StoreId;
      tblAdds.SubLocationId = Obj.SubLocationId;
      tblAdds.UniqueId = uId;
      tblAdds.ReassignDate = CurrentIndianDate();

      _db.tbl_Adds.Add(tblAdds);

      int status = _db.SaveChanges();

      AddsLocationMapping(Obj, tblAdds.Id);
      AddsSubLocationMapping(Obj, tblAdds.Id);

      return status;

    }

    public int UpdateAdd(Advertisement Obj)
    {
      tbl_Adds tblAdds = (from a in _db.tbl_Adds where a.Id == Obj.Id select a).SingleOrDefault();

      tblAdds.Name = Obj.Name;
      tblAdds.ImagePath = Obj.ImagePath;
      tblAdds.Description = Obj.Description;
      tblAdds.RewardPoints = Obj.RewardPoints;
      tblAdds.LocationId = Obj.LocationId;
      tblAdds.LocationName = Obj.LocationName;
      tblAdds.StoreId = Obj.StoreId;
      tblAdds.SubLocationId = Obj.SubLocationId;

      tblAdds.IsActive = 1;
      tblAdds.Time = Obj.Time;
      tblAdds.ViewsLimit = Obj.ViewsLimit;

      int status = _db.SaveChanges();

      AddsLocationMapping(Obj, tblAdds.Id);
      AddsSubLocationMapping(Obj, tblAdds.Id);

      return status;
    }

    public int DeleteAdd(long Id)
    {
      tbl_Adds rec = (from a in _db.tbl_Adds where a.Id == Id select a).SingleOrDefault();
      rec.IsActive = 0;

      _db.tbl_UserActivity.RemoveRange(_db.tbl_UserActivity.Where(x => x.Type == "ADD" && x.AddId == rec.Id && x.IsViewed == 0 && x.IsLocked != 1));

      return _db.SaveChanges();
    }

    public int Reassign(long Id)
    {
      tbl_Adds tblAdds = new tbl_Adds();
      tbl_Adds Obj = (from a in _db.tbl_Adds where a.Id == Id select a).SingleOrDefault();


      tblAdds.Name = Obj.Name;
      tblAdds.ImagePath = Obj.ImagePath;
      tblAdds.Description = Obj.Description;
      tblAdds.RewardPoints = Obj.RewardPoints;
      tblAdds.LocationId = Obj.LocationId;
      tblAdds.LocationName = Obj.LocationName;
      tblAdds.CreatedAt = Obj.CreatedAt;
      tblAdds.IsActive = 1;
      tblAdds.Time = Obj.Time;
      tblAdds.ViewsLimit = Obj.ViewsLimit;
      tblAdds.StoreId = Obj.StoreId;
      tblAdds.SubLocationId = Obj.SubLocationId;
      tblAdds.UniqueId = Obj.UniqueId;
      tblAdds.ReassignDate = CurrentIndianDate();

      _db.tbl_Adds.Add(tblAdds);

      //Inactivate Old Add
      Obj.IsActive = 0;

      int i = _db.SaveChanges();

      _db.tbl_UserActivity.RemoveRange(_db.tbl_UserActivity.Where(x => x.Type == "ADD" && x.AddId == Obj.Id && x.IsViewed == 0 && x.IsLocked != 1));

      var tblLoc = (from a in _db.tbl_Adds_Location_Mapping where a.AddId == Id select a).ToList();

      foreach(var Item in tblLoc)
      {
        Item.AddId = tblAdds.Id;
        
      }

      var tblSubLoc = (from a in _db.tbl_Adds_Sublocation_Mapping where a.AddId == Id select a).ToList();

      foreach (var Item in tblSubLoc)
      {
        Item.AddId = tblAdds.Id;
        
      }

      _db.SaveChanges();

      return i;
    }

    public List<Advertisement> GetAdds(string CustomerId)
    {
      var data = (from a in _db.tbl_Adds
                  join
                  b in _db.tbl_Location on a.LocationId equals b.Id
                  join
                   c in _db.tbl_Stores on a.StoreId equals c.Id
                  where a.IsActive == 1 && c.UniqueId == CustomerId
                  select a).ToList();

      return (from a in data
              orderby a.Id descending
              select new Advertisement
              {
                Id = a.Id,
                Name = a.Name,
                Description = a.Description,
                ImagePath = a.ImagePath,
                RewardPoints = a.RewardPoints,
                LocationId = a.LocationId,
                LocationName = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).First() + "",
                Time = a.Time,
                ViewsLimit = a.ViewsLimit,
                strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                SubLocationId = a.SubLocationId,
                StoreId = a.StoreId
              }).ToList();
    }

    public List<Advertisement> GetAdds(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_Adds
                  
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                  && (LocationId == 0 || a.LocationId == LocationId)
                  && (Status == "" || (Status == "Open" ? a.ViewsLimit > 0 : a.ViewsLimit == 0))
                  select a).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new Advertisement
                         {
                           Id = a.Id,
                           Name = a.Name,
                           Description = a.Description,
                           ImagePath = a.ImagePath,
                           RewardPoints = a.RewardPoints,
                           LocationId = a.LocationId,
                           LocationName = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).FirstOrDefault() + "",
                           Time = a.Time,
                           ViewsLimit = a.ViewsLimit,
                           strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                           Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                           SubLocationId = a.SubLocationId,
                           StoreId = a.StoreId,
                           StoreName = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           CustomerName = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers  where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();


      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    public List<RptAdvertisement> GetAddsReport(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_Adds
                  join
                  b in _db.tbl_Location on a.LocationId equals b.Id
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                  && (LocationId == 0 || a.LocationId == LocationId)
                  && (Status == "" || (Status == "Open" ? a.ViewsLimit > 0 : a.ViewsLimit == 0))
                  select a).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new RptAdvertisement
                         {

                           Title = a.Name,
                           Description = a.Description,
                           RewardPoints = a.RewardPoints,
                           Location = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).First() + "",
                           Time = a.Time,
                           ViewsLeft = a.ViewsLimit,
                           DateofUpload = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                           Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                           //Store = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           Customer = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();

      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    public List<CustomerActivityReport> GetAddsReportCustom(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_Adds
                  join
                  b in _db.tbl_Location on a.LocationId equals b.Id
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                  && (LocationId == 0 || a.LocationId == LocationId)
                  && (Status == "" || (Status == "Open" ? a.ViewsLimit > 0 : a.ViewsLimit == 0))
                  select a).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new CustomerActivityReport
                         {
                           Type = "Advertisement",
                           Title = a.Name,
                           Description = a.Description,
                           RewardPoints = a.RewardPoints.Value,
                           Location = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).First() + "",
                           ViewsLeft = a.ViewsLimit.Value,
                           DateofUpload = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                           Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                           Store = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           Customer = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();


      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }

    public void AddsLocationMapping(Advertisement a, long AddId)
    {
      try
      {
        var tbl = (from x in _db.tbl_Adds_Location_Mapping where x.AddId == AddId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Adds_Location_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Adds_Location_Mapping tblObj = new tbl_Adds_Location_Mapping();

        string[] strArray = (a.AddLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.AddId = AddId;
          tblObj.LocationId = long.Parse(item);
          _db.tbl_Adds_Location_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }
      

    }

    public void AddsSubLocationMapping(Advertisement a, long AddId)
    {
      try
      {
        LocationMapper _locationMapper = new LocationMapper();

        var tbl = (from x in _db.tbl_Adds_Sublocation_Mapping where x.AddId == AddId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Adds_Sublocation_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Adds_Sublocation_Mapping tblObj = new tbl_Adds_Sublocation_Mapping();


        string[] strArray = (a.AddSubLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.AddId = AddId;
          tblObj.SubLocationId = long.Parse(item);
          tblObj.LocationId = _locationMapper.SingleSubLocation(long.Parse(item)).LocationId;
          _db.tbl_Adds_Sublocation_Mapping.Add(tblObj);
          _db.SaveChanges();
        }

      }
      catch
      {

      }
    
    }


    public List<Location> GetAddsLocations(long AddId)
    {

      return (from c in _db.tbl_Location
              join p in (from a in _db.tbl_Adds_Location_Mapping where a.AddId == AddId select a) on c.Id equals p.LocationId into ps
              from p in ps.DefaultIfEmpty()
              select new Location { Id = c.Id, Name = c.Name, Flag = p.AddId == null ? false : true }).ToList();



    }

    public List<SubLocation> GetAddsSubLocations(List<long> LocationId, long AddId)
    {

      return (from c in _db.tbl_SubLocation
              join p in (from a in _db.tbl_Adds_Sublocation_Mapping where a.AddId == AddId select a) on c.Id equals p.SubLocationId into ps
              from p in ps.DefaultIfEmpty()
              where LocationId.Contains(c.LocationId.Value)
              select new SubLocation { Id = c.Id, Name = c.Name, Flag = p.AddId == null ? false : true }).ToList();



    }


  }
}