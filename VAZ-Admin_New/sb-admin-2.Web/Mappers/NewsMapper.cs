﻿using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Mappers
{
  public class NewsMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    public List<NewsDTO> GetNewsList()
    {
      var data = (from a in _db.tbl_News
                  
                
                  join d in _db.tbl_OfferCategories on a.CategoryId equals d.Id
                  where a.IsActive == true
                  select new
                  {
                    a.Id,
                    a.Title,
                    a.News,
                    a.ExpiryDate,
                    a.SubLocationId,
                    a.LocationId,
                    a.CreatedAt,
                    a.CreatedBy,
                    a.LastUpdatedAt,
                    a.LastUpdatedBy,
                    a.IsActive,
                    a.IsHeadline,
                 
                    a.IsClassified,
                    a.CategoryId,
                    Category = d.Name
                  }).ToList();

      return (from a in data
              orderby a.Id descending
              select new NewsDTO
              {
                Id = a.Id,
                Title = a.Title,
                News = a.News,
                strExpiryDate = a.ExpiryDate.Value.ToString("dd/MM/yyyy"),
                strCreatedAt = a.CreatedAt.Value.ToString("dd/MM/yyyy"),

                SubLocationId = a.SubLocationId,
               
                LocationId = a.LocationId,
                IsActive = a.IsActive,
                IsHeadline = a.IsHeadline ?? false,
                IsClassified = a.IsClassified ?? false,
                Status = a.ExpiryDate.Value >= DateTime.Now ? "Open" : "Closed",
                CategoryId = a.CategoryId,
                Category = a.Category
              }).ToList();
    }

    public NewsDTO GetSingleNewsObj(long uId)
    {
      var data = (from a in _db.tbl_News
                
                 
                  join d in _db.tbl_OfferCategories on a.CategoryId equals d.Id
                  where a.Id == uId
                  select new
                  {
                    a.Id,
                    a.Title,
                    a.News,
                    a.ExpiryDate,
                    a.SubLocationId,
                    a.LocationId,
                    a.CreatedAt,
                    a.CreatedBy,
                    a.LastUpdatedAt,
                    a.LastUpdatedBy,
                    a.IsActive,
                    a.IsHeadline,
                   
                   a.IsClassified,
                    a.CategoryId,
                    Category = d.Name
                  }).ToList();

      return (from a in data
              orderby a.Id descending
              select new NewsDTO
              {
                Id = a.Id,
                Title = a.Title,
                News = a.News,
                strExpiryDate = a.ExpiryDate.Value.ToString("dd/MM/yyyy"),
                strCreatedAt = a.CreatedAt.Value.ToString("dd/MM/yyyy"),
              
                SubLocationId = a.SubLocationId,
              
                LocationId = a.LocationId,
                IsActive = a.IsActive,
                IsHeadline = a.IsHeadline ?? false,
                IsClassified = a.IsClassified ?? false,
                Status = a.ExpiryDate.Value >= DateTime.Now ? "Open" : "Closed",
                CategoryId = a.CategoryId,
                Category = a.Category
               
              }).SingleOrDefault();
    }

    public long PostNews(NewsDTO nObj)
    {
      tbl_News tblObj = new tbl_News();

      tblObj.Title = nObj.Title;
      tblObj.News = nObj.News;
      tblObj.ExpiryDate = DateTime.ParseExact(nObj.strExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      tblObj.IsHeadline = nObj.IsHeadline;
      tblObj.LocationId = nObj.LocationId;
      tblObj.SubLocationId = nObj.SubLocationId;
      tblObj.IsHeadline = nObj.IsHeadline;
      tblObj.IsActive = true;
      tblObj.CreatedAt = CurrentIndianDate();
      tblObj.CategoryId = nObj.CategoryId;
      tblObj.IsClassified = nObj.IsClassified;

      _db.tbl_News.Add(tblObj);
      _db.SaveChanges();

      NewsLocationMapping(nObj, tblObj.Id);
      NewsSubLocationMapping(nObj, tblObj.Id);

      return tblObj.Id;
    }

    public int UpdateNews(NewsDTO nObj)
    {
      tbl_News tblObj = (from a in _db.tbl_News where a.Id == nObj.Id select a).SingleOrDefault();

      tblObj.Title = nObj.Title;
      tblObj.News = nObj.News;
      tblObj.ExpiryDate = DateTime.ParseExact(nObj.strExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      tblObj.IsHeadline = nObj.IsHeadline;
      tblObj.LocationId = nObj.LocationId;
      tblObj.SubLocationId = nObj.SubLocationId;
      tblObj.IsHeadline = nObj.IsHeadline;
      tblObj.LastUpdatedAt = CurrentIndianDate();
      tblObj.CategoryId = nObj.CategoryId;
      tblObj.IsClassified = nObj.IsClassified;

      int status =  _db.SaveChanges();

      NewsLocationMapping(nObj, tblObj.Id);
      NewsSubLocationMapping(nObj, tblObj.Id);

      return status;
    }

    public int DeleteNews(long uId)
    {
      tbl_News rec = (from a in _db.tbl_News where a.Id == uId select a).SingleOrDefault();

      _db.tbl_News.Remove(rec);
      return _db.SaveChanges();
    }

    public List<NewsImages> GetNewsImages(long NewsId)
    {
      return (from a in _db.tbl_NewsImages
              where a.NewsId == NewsId
              select new NewsImages
              {
                Id = a.Id,
                NewsId = a.NewsId,
                ImagePath = a.ImagePath
              }).ToList();

    }

    public int UploadMultipleNewsImages(NewsImages iObj)
    {
      tbl_NewsImages tbl = new tbl_NewsImages();
      tbl.NewsId = iObj.NewsId;
      tbl.ImagePath = iObj.ImagePath;
      _db.tbl_NewsImages.Add(tbl);
      return _db.SaveChanges();
    }

    public int DeleteNewsImage(long Id)
    {
      tbl_NewsImages rec = (from a in _db.tbl_NewsImages where a.Id == Id select a).SingleOrDefault();
      _db.tbl_NewsImages.Remove(rec);
      return _db.SaveChanges();
    }

    public int NewsImageCount(long NewsId)
    {
      int count = (from a in _db.tbl_NewsImages where a.NewsId == NewsId select a).Count();
      return count;
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }

    public string getNewsSublocationIds(long NewsId, long LocationId)
    {
      string Sublocations = "";
      var data = (from a in _db.tbl_News_Sublocation_Mapping where a.NewsId == NewsId && a.LocationId == LocationId select a.SublocationId).ToList();

      foreach (var item in data)
      {
        Sublocations = Sublocations + "," + item;
      }

      return Sublocations.Trim(',');

    }

   

    public void NewsLocationMapping(NewsDTO a, long NewsId)
    {
      try
      {
        var tbl = (from x in _db.tbl_News_Location_Mapping where x.NewsId == NewsId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_News_Location_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_News_Location_Mapping tblObj = new tbl_News_Location_Mapping();

        string[] strArray = (a.NewsLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.NewsId = NewsId;
          tblObj.LocationId = long.Parse(item);
          _db.tbl_News_Location_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }

    }

    public void NewsSubLocationMapping(NewsDTO a, long NewsId)
    {
      try
      {
        LocationMapper _locationMapper = new LocationMapper();

        var tbl = (from x in _db.tbl_News_Sublocation_Mapping where x.NewsId == NewsId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_News_Sublocation_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_News_Sublocation_Mapping tblObj = new tbl_News_Sublocation_Mapping();


        string[] strArray = (a.NewsSublocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.NewsId = NewsId;
          tblObj.SublocationId = long.Parse(item);
          tblObj.LocationId = _locationMapper.SingleSubLocation(long.Parse(item)).LocationId;
          _db.tbl_News_Sublocation_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }

    }


    public List<Location> GetNewsLocations(long NewsId)
    {

      return (from c in _db.tbl_Location
              join p in (from a in _db.tbl_News_Location_Mapping where a.NewsId == NewsId select a) on c.Id equals p.LocationId into ps
              from p in ps.DefaultIfEmpty()
              select new Location { Id = c.Id, Name = c.Name, Flag = p.NewsId == null ? false : true }).ToList();



    }

    public List<SubLocation> GetNewsSubLocations(List<long> LocationId, long NewsId)
    {

      return (from c in _db.tbl_SubLocation
              join p in (from a in _db.tbl_News_Sublocation_Mapping where a.NewsId == NewsId select a) on c.Id equals p.SublocationId into ps
              from p in ps.DefaultIfEmpty()
              where LocationId.Contains(c.LocationId.Value)
              select new SubLocation { Id = c.Id, Name = c.Name, Flag = p.NewsId == null ? false : true }).ToList();



    }


  }
}