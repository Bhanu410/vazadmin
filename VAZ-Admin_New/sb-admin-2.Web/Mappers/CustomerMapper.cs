﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System.Globalization;

namespace sb_admin_2.Web.Mappers
{
  public class CustomerMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    public Customer SingleCustomer(long Id)
    {
      return (from a in _db.tbl_Customers
              where a.Id == Id
              select new Customer
              {
                Id = a.Id,
                CustomerId = a.CustomerId,
                Name = a.Name,
                CompanyName = a.ComapanyName,
                LocationId = a.LocationId,
                SubLocationId = a.SubLocationId,
                Mobile = a.Mobile,
                Email = a.Email,
                CreatedAt = a.CreatedAt,
                IsActive = a.IsActive,
                LastUpdatedAt = a.LastUpdatedAt,
                ImagePath = a.ImagePath
              }).SingleOrDefault();
    }

    public Customer SingleCustomer(string CustomerId)
    {
      return (from a in _db.tbl_Customers
              where a.CustomerId == CustomerId
              select new Customer
              {
                Id = a.Id,
                CustomerId = a.CustomerId,
                Name = a.Name,
                CompanyName = a.ComapanyName,
                LocationId = a.LocationId,
                SubLocationId = a.SubLocationId,
                Mobile = a.Mobile,
                Email = a.Email,
                CreatedAt = a.CreatedAt,
                IsActive = a.IsActive,
                LastUpdatedAt = a.LastUpdatedAt,
                ImagePath = a.ImagePath
              }).SingleOrDefault();
    }

    public List<Customer> CustomerList(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      return (from a in _db.tbl_Customers
              join c in _db.tbl_Location on a.LocationId equals c.Id
              join d in _db.tbl_SubLocation on a.SubLocationId equals d.Id
              where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                      && (LocationId == 0 || a.LocationId == LocationId)
                      && (CustomerId == "" || a.CustomerId == CustomerId)
              orderby a.Id descending
              select new Customer
              {
                Id = a.Id,
                CustomerId = a.CustomerId,
                Name = a.Name,
                CompanyName = a.ComapanyName,
                LocationId = a.LocationId,
                SubLocationId = a.SubLocationId,
                Mobile = a.Mobile,
                Email = a.Email,
                CreatedAt = a.CreatedAt,
                IsActive = a.IsActive,
                LastUpdatedAt = a.LastUpdatedAt,
                ImagePath = a.ImagePath,
                LocationName = c.Name,
                SubLocationName = d.Name
              }).ToList();
    }

    public List<CustomerRpt> CustomerListRpt(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      return (from a in _db.tbl_Customers
              join c in _db.tbl_Location on a.LocationId equals c.Id
              join d in _db.tbl_SubLocation on a.SubLocationId equals d.Id
              where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                      && (LocationId == 0 || a.LocationId == LocationId)
                      && (CustomerId == "" || a.CustomerId == CustomerId)
              orderby a.Id descending
              select new CustomerRpt
              {
                CustomerId = a.CustomerId,
                Name = a.Name,
                CompanyName = a.ComapanyName,
                Mobile = a.Mobile,
                Email = a.Email,
                Location = c.Name,
                SubLocation = d.Name,
                RegisteredOn = a.CreatedAt
              }).ToList();
    }


    public List<Customer> CustomerList(long LocationId, long SubLocationId)
    {
      return (from a in _db.tbl_Customers
              join c in _db.tbl_Location on a.LocationId equals c.Id
              join d in _db.tbl_SubLocation on a.SubLocationId equals d.Id
              where a.IsActive == 1 && a.LocationId == LocationId && a.SubLocationId == SubLocationId
              orderby a.Id descending
              select new Customer
              {
                Id = a.Id,
                CustomerId = a.CustomerId,
                Name = a.Name,
                CompanyName = a.ComapanyName,
                LocationId = a.LocationId,
                SubLocationId = a.SubLocationId,
                Mobile = a.Mobile,
                Email = a.Email,
                CreatedAt = a.CreatedAt,
                IsActive = a.IsActive,
                LastUpdatedAt = a.LastUpdatedAt,
                ImagePath = a.ImagePath,
                LocationName = c.Name,
                SubLocationName = d.Name
              }).ToList();
    }


    public int AddCustomer(Customer sObj)
    {
      LocationMapper _locationMapper = new LocationMapper();
      string LocationCode = "";
      string ComapanyCode = "";
      string CustomerId = "";

      LocationCode = (_locationMapper.SingleLocation(sObj.LocationId.Value).Name).Substring(0, 3).ToUpper();
      ComapanyCode = sObj.CompanyName.Substring(0, 2).ToUpper();
      //CustomerId = "IN" + LocationCode + ComapanyCode + GenerateCustomerId();
      CustomerId = GenerateCustomerId();

      tbl_Customers tblCustomers = new tbl_Customers();
      tblCustomers.CustomerId = CustomerId;
      tblCustomers.Name = sObj.Name;
      tblCustomers.ComapanyName = sObj.CompanyName;
      tblCustomers.LocationId = sObj.LocationId;
      tblCustomers.SubLocationId = sObj.SubLocationId;
      tblCustomers.ImagePath = sObj.ImagePath;
      tblCustomers.CreatedAt = DateTime.Now;
      tblCustomers.IsActive = 1;
      tblCustomers.Email = sObj.Email;
      tblCustomers.Mobile = sObj.Mobile;


      _db.tbl_Customers.Add(tblCustomers);

      return _db.SaveChanges();
    }

    public int UpdateCustomer(Customer sObj)
    {
      tbl_Customers tblCustomers = (from a in _db.tbl_Customers where a.Id == sObj.Id select a).SingleOrDefault();

      tblCustomers.Name = sObj.Name;
      tblCustomers.LocationId = sObj.LocationId;
      tblCustomers.SubLocationId = sObj.SubLocationId;
      tblCustomers.ImagePath = sObj.ImagePath;
      tblCustomers.Email = sObj.Email;
      tblCustomers.Mobile = sObj.Mobile;
      tblCustomers.ComapanyName = sObj.CompanyName;

      return _db.SaveChanges();

    }

    public int DeleteCustomer(long Id)
    {
      tbl_Customers tblCustomers = (from a in _db.tbl_Customers where a.Id == Id select a).SingleOrDefault();

      tblCustomers.IsActive = 0;

      return _db.SaveChanges();
    }




    private string GenerateCustomerId()
    {
      string uId = Guid.NewGuid().ToString();
      uId = uId.Substring(0, 8).ToUpper();

      var data = (from a in _db.tbl_Customers where a.CustomerId == uId select a).ToList();

      if (data.Count > 0)
      {
        uId = GenerateCustomerId();
      }

      return uId;
    }

  }
}