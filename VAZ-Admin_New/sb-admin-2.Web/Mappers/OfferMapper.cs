﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System.Globalization;

namespace sb_admin_2.Web.Mappers
{
    public class OfferMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        #region Category

        public OfferCategories GetSingleCategory(long CategoryId)
        {
            return (from a in _db.tbl_OfferCategories
                    where a.Id == CategoryId
                    orderby a.Id descending
                    select new OfferCategories { Id = a.Id, Name = a.Name, ImagePath = a.ImagePath }).SingleOrDefault();
        }

        public List<OfferCategories> GetCategories()
        {
            return (from a in _db.tbl_OfferCategories
                    where a.IsActive == 1
                    orderby a.Id descending
                    select new OfferCategories { Id = a.Id, Name = a.Name, ImagePath = a.ImagePath }).ToList();
        }

        public List<OfferCategories> GetCategoriesByName(string CategoryName)
        {
            return (from a in _db.tbl_OfferCategories
                    where a.IsActive == 1 && a.Name == CategoryName
                    orderby a.Id descending
                    select new OfferCategories { Id = a.Id, Name = a.Name, ImagePath = a.ImagePath }).ToList();
        }

        public int PostCategories(OfferCategories CatObj)
        {
            tbl_OfferCategories tblCatObj = new tbl_OfferCategories();
            tblCatObj.Name = CatObj.Name;
            tblCatObj.ImagePath = CatObj.ImagePath;
            tblCatObj.UniqueName = Guid.NewGuid().ToString();
            tblCatObj.IsActive = 1;

            _db.tbl_OfferCategories.Add(tblCatObj);
            return _db.SaveChanges();
        }

        public int UpdateCategories(OfferCategories CatObj)
        {
            tbl_OfferCategories tblCatObj = (from a in _db.tbl_OfferCategories where a.Id == CatObj.Id select a).SingleOrDefault();
            tblCatObj.Name = CatObj.Name;
            tblCatObj.ImagePath = CatObj.ImagePath;

            return _db.SaveChanges();
        }

        public int DeleteCategories(long CategoryId)
        {
            tbl_OfferCategories rec = (from a in _db.tbl_OfferCategories where a.Id == CategoryId select a).SingleOrDefault();

            _db.tbl_OfferCategories.Remove(rec);
            return _db.SaveChanges();
        }

        #endregion

        #region SubCategory

        public OfferSubCategories GetSingleSubCategory(long SubCatId)
        {
            return (from a in _db.tbl_OfferSubCategories
                    where a.Id == SubCatId
                    select new OfferSubCategories
                    {
                        Id = a.Id,
                        Name = a.Name,
                        ImagePath = a.ImagePath,
                        MasterId = a.MasterId,
                        LocationId = a.LocationId.Value,
                        SubLocationId = a.SubLocationId.Value,
                        Address = a.Address
                    }).SingleOrDefault();
        }

        public List<OfferSubCategories> GetSubCategories()
        {
            return (from a in _db.tbl_OfferSubCategories
                    join b in _db.tbl_OfferCategories on a.MasterId equals b.Id
                    where a.IsActive == 1
                    orderby a.Id descending
                    select new OfferSubCategories
                    {
                        Id = a.Id,
                        Name = a.Name,
                        ImagePath = a.ImagePath,
                        Category = b.Name,
                        LocationId = a.LocationId.Value,
                        SubLocationId = a.SubLocationId.Value
                    }).ToList();
        }

        public List<OfferSubCategories> GetSubCategoriesByName(string SubCategoryName, long MasterId)
        {
            return (from a in _db.tbl_OfferSubCategories
                   
                    where a.IsActive == 1 && a.Name == SubCategoryName && a.MasterId == MasterId
                    orderby a.Id descending
                    select new OfferSubCategories
                    {
                        Id = a.Id,
                        Name = a.Name,
                        ImagePath = a.ImagePath,
                       
                        LocationId = a.LocationId.Value,
                        SubLocationId = a.SubLocationId.Value
                    }).ToList();
        }

        public List<OfferSubCategories> GetSubCategoriesByCatId(long CategoryId)
        {
            return (from a in _db.tbl_OfferSubCategories
                    join b in _db.tbl_OfferCategories on a.MasterId equals b.Id
                    where a.IsActive == 1 && a.MasterId == CategoryId
                    orderby a.MasterId descending, a.Id descending
                    select new OfferSubCategories
                    {
                        Id = a.Id,
                        Name = a.Name,
                        ImagePath = a.ImagePath,
                        Category = b.Name,
                        LocationId = a.LocationId.Value,
                        SubLocationId = a.SubLocationId.Value
                    }).ToList();
        }

        public int PostSubCategories(OfferSubCategories SubCatObj)
        {
            tbl_OfferSubCategories tblSubCatObj = new tbl_OfferSubCategories();
            tblSubCatObj.MasterId = SubCatObj.MasterId;
            tblSubCatObj.Name = SubCatObj.Name;
            tblSubCatObj.ImagePath = SubCatObj.ImagePath;
            tblSubCatObj.UniqueName = Guid.NewGuid().ToString();
            tblSubCatObj.IsActive = 1;
            tblSubCatObj.LocationId = SubCatObj.LocationId;
            tblSubCatObj.SubLocationId = SubCatObj.SubLocationId;
            tblSubCatObj.Address = SubCatObj.Address;

            _db.tbl_OfferSubCategories.Add(tblSubCatObj);
            return _db.SaveChanges();
        }

        public int UpdateSubCategories(OfferSubCategories SubCatObj)
        {
            tbl_OfferSubCategories tblSubCatObj = (from a in _db.tbl_OfferSubCategories where a.Id == SubCatObj.Id select a).SingleOrDefault();

            tblSubCatObj.Name = SubCatObj.Name;
            tblSubCatObj.ImagePath = SubCatObj.ImagePath;
            tblSubCatObj.LocationId = SubCatObj.LocationId;
            tblSubCatObj.SubLocationId = SubCatObj.SubLocationId;
            tblSubCatObj.Address = SubCatObj.Address;
            tblSubCatObj.MasterId = SubCatObj.MasterId;

            return _db.SaveChanges();
        }

        public int DeleteSubCategories(long SubCatId)
        {
            tbl_OfferSubCategories rec = (from a in _db.tbl_OfferSubCategories where a.Id == SubCatId select a).SingleOrDefault();

            _db.tbl_OfferSubCategories.Remove(rec);
            return _db.SaveChanges();
        }

        #endregion

        #region Offers

        public List<Offers> GetOffers()
        {
            var data = (from a in _db.tbl_Offers
                        join b in _db.tbl_OfferSubCategories on a.SubCategoryId equals b.Id
                        join c in _db.tbl_OfferCategories on a.CategoryId equals c.Id
                        join d in _db.tbl_Stores on a.StoreId equals d.Id
                        where a.IsActive == 1
                        orderby a.Id descending
                        select new Offers
                        {
                            Id = a.Id,
                            ProductName = a.ProductName,
                            ImagePath = a.ImagePath,
                            OfferPercent = a.OfferPercent,
                            OfferStartDate = a.OfferStartDate,
                            OfferEndDate = a.OfferEndDate,
                            Description = a.Description,


                            ImageTwo = a.ImageTwo,
                            ImageThree = a.ImageThree,
                            ImageFour = a.ImageFour,
                            ImageFive = a.ImageFive,
                            ImageSix = a.ImageSix,

                            CreatedAt = a.CreatedAt,
                            Category = c.Name,
                            SubCategory = b.Name,
                            Status = a.OfferEndDate >= DateTime.Now ? "Open" : "Closed",
                            LocationId = a.LocationId,
                            SubLocationId = a.SubLocationId,
                            StoreId = a.StoreId,
                            Address = a.Address,
                            StoreName = d.Name
                        }).ToList();

            return (from a in data
                    select new Offers
                    {
                        Id = a.Id,
                        ProductName = a.ProductName,
                        ImagePath = a.ImagePath,
                        OfferPercent = a.OfferPercent,
                        strOfferStartDate = a.OfferStartDate.Value.ToString("ddMMMyyyy"),
                        strOfferEndDate = a.OfferEndDate.Value.ToString("dd MMM yyyy"),
                        Description = a.Description,


                        ImageTwo = a.ImageTwo,
                        ImageThree = a.ImageThree,
                        ImageFour = a.ImageFour,
                        ImageFive = a.ImageFive,
                        ImageSix = a.ImageSix,

                        CreatedAt = a.CreatedAt,
                        Category = a.Category,
                        SubCategory = a.SubCategory,
                        Status = a.OfferEndDate >= DateTime.Now ? "Open" : "Closed",
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        StoreId = a.StoreId,
                        Address = a.Address,
                        StoreName = a.StoreName
                    }).ToList();
        }

        public List<Offers> GetOffers(string CustomerId)
        {
            var data = (from a in _db.tbl_Offers
                        join b in _db.tbl_OfferSubCategories on a.SubCategoryId equals b.Id
                        join c in _db.tbl_OfferCategories on a.CategoryId equals c.Id
                        join
                         d in _db.tbl_Stores on a.StoreId equals d.Id
                        where a.IsActive == 1 && d.UniqueId == CustomerId
                        orderby a.Id descending
                        select new Offers
                        {
                            Id = a.Id,
                            ProductName = a.ProductName,
                            ImagePath = a.ImagePath,
                            OfferPercent = a.OfferPercent,
                            OfferStartDate = a.OfferStartDate,
                            OfferEndDate = a.OfferEndDate,
                            Description = a.Description,


                            ImageTwo = a.ImageTwo,
                            ImageThree = a.ImageThree,
                            ImageFour = a.ImageFour,
                            ImageFive = a.ImageFive,
                            ImageSix = a.ImageSix,

                            CreatedAt = a.CreatedAt,
                            Category = c.Name,
                            SubCategory = b.Name,
                            Status = a.OfferEndDate >= DateTime.Now ? "Open" : "Closed",
                            LocationId = a.LocationId,
                            SubLocationId = a.SubLocationId,
                            StoreId = a.StoreId,
                            Address = a.Address,
                            StoreName = d.Name,
                            CustomerId = d.UniqueId
                        }).ToList();

            return (from a in data
                    select new Offers
                    {
                        Id = a.Id,
                        ProductName = a.ProductName,
                        ImagePath = a.ImagePath,
                        OfferPercent = a.OfferPercent,
                        strOfferStartDate = a.OfferStartDate.Value.ToString("dd MMM yyyy"),
                        strOfferEndDate = a.OfferEndDate.Value.ToString("dd MMM yyyy"),
                        Description = a.Description,


                        ImageTwo = a.ImageTwo,
                        ImageThree = a.ImageThree,
                        ImageFour = a.ImageFour,
                        ImageFive = a.ImageFive,
                        ImageSix = a.ImageSix,

                        CreatedAt = a.CreatedAt,
                        Category = a.Category,
                        SubCategory = a.SubCategory,
                        Status = a.OfferEndDate >= DateTime.Now ? "Open" : "Closed",
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        StoreId = a.StoreId,
                        Address = a.Address,
                        StoreName = a.StoreName,
                        CustomerId = a.CustomerId
                    }).ToList();
        }

        public long PostOffers(Offers Obj)
        {
            tbl_Offers tblOffer = new tbl_Offers();

         

            tblOffer.CategoryId = Obj.CategoryId;
            tblOffer.SubCategoryId = Obj.SubCategoryId;
            tblOffer.ProductName = Obj.ProductName;
            tblOffer.ImagePath = Obj.ImagePath;
            tblOffer.OfferPercent = Obj.OfferPercent;
            tblOffer.OfferStartDate = DateTime.ParseExact( Obj.strOfferStartDate,"dd/MM/yyyy", CultureInfo.InvariantCulture);
            tblOffer.OfferEndDate = DateTime.ParseExact(Obj.strOfferEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            tblOffer.Description = Obj.Description;


            tblOffer.ImageTwo = Obj.ImageTwo;
            tblOffer.ImageThree = Obj.ImageThree;
            tblOffer.ImageFour = Obj.ImageFour;
            tblOffer.ImageFive = Obj.ImageFive;
            tblOffer.ImageSix = Obj.ImageSix;

            tblOffer.IsActive = 1;
            tblOffer.CreatedAt = DateTime.Now;
            tblOffer.LastUpdatedAt = DateTime.Now;
            tblOffer.LocationId = Obj.LocationId;
            tblOffer.SubLocationId = Obj.SubLocationId;
            tblOffer.StoreId = Obj.StoreId;
            tblOffer.Address = Obj.Address;

            _db.tbl_Offers.Add(tblOffer);
            _db.SaveChanges();

            return tblOffer.Id;
        }

        public int UpdateOffers(Offers Obj)
        {
            tbl_Offers tblOffer = (from a in _db.tbl_Offers where a.Id == Obj.Id select a).SingleOrDefault();

            tblOffer.CategoryId = Obj.CategoryId;
            tblOffer.SubCategoryId = Obj.SubCategoryId;
            tblOffer.ProductName = Obj.ProductName;
            tblOffer.ImagePath = Obj.ImagePath;
            tblOffer.OfferPercent = Obj.OfferPercent;
            tblOffer.OfferStartDate = DateTime.ParseExact(Obj.strOfferStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            tblOffer.OfferEndDate = DateTime.ParseExact(Obj.strOfferEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            tblOffer.Description = Obj.Description;


            tblOffer.ImageTwo = Obj.ImageTwo;
            tblOffer.ImageThree = Obj.ImageThree;
            tblOffer.ImageFour = Obj.ImageFour;
            tblOffer.ImageFive = Obj.ImageFive;
            tblOffer.ImageSix = Obj.ImageSix;

            tblOffer.LastUpdatedAt = DateTime.Now;
            tblOffer.LocationId = Obj.LocationId;
            tblOffer.SubLocationId = Obj.SubLocationId;
            tblOffer.StoreId = Obj.StoreId;
            tblOffer.Address = Obj.Address;

            return _db.SaveChanges();
        }

        public int DeleteOffers(long OfferId)
        {
            tbl_Offers rec = (from a in _db.tbl_Offers where a.Id == OfferId select a).SingleOrDefault();

            _db.tbl_Offers.Remove(rec);
            return _db.SaveChanges();
        }


        internal Offers GetSingleOffer(long OfferId)
        {
            var data = (from a in _db.tbl_Offers
                        where a.Id == OfferId
                        select a).ToList();

            return (from a in data
                    select new Offers
                    {
                        Id = a.Id,
                        CategoryId = a.CategoryId,
                        SubCategoryId = a.SubCategoryId,
                        ProductName = a.ProductName,
                        ImagePath = a.ImagePath,
                        OfferPercent = a.OfferPercent,
                        OfferStartDate = a.OfferStartDate,
                        OfferEndDate = a.OfferEndDate,
                        Description = a.Description,

                        ImageTwo = a.ImageTwo,
                        ImageThree = a.ImageThree,
                        ImageFour = a.ImageFour,
                        ImageFive = a.ImageFive,
                        ImageSix = a.ImageSix,

                        CreatedAt = a.CreatedAt,
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        StoreId = a.StoreId,
                        Address = a.Address,

                        strOfferStartDate = a.OfferStartDate.Value.ToString("dd/MM/yyyy"),
                        strOfferEndDate = a.OfferEndDate.Value.ToString("dd/MM/yyyy")

                    }).SingleOrDefault();
        }

        public List<OfferImages> GetOfferImages(long OfferId)
        {
            return (from a in _db.tbl_OfferImages
                    where a.OfferId == OfferId
                    select new OfferImages
                    {
                        Id = a.Id,
                        OfferId = a.OfferId,
                        ImagePath = a.ImagePath
                    }).ToList();

        }

        public int UploadMultipleOfferImages(OfferImages iObj)
        {
            tbl_OfferImages tbl = new tbl_OfferImages();
            tbl.OfferId = iObj.OfferId;
            tbl.ImagePath = iObj.ImagePath;
            _db.tbl_OfferImages.Add(tbl);
            return _db.SaveChanges();
        }

        public int DeleteOfferImage(long Id)
        {
            tbl_OfferImages rec = (from a in _db.tbl_OfferImages where a.Id == Id select a).SingleOrDefault();
            _db.tbl_OfferImages.Remove(rec);
            return _db.SaveChanges();
        }

        public int OfferImageCount(long OfferId)
        {
            int count = (from a in _db.tbl_OfferImages where a.OfferId == OfferId select a).Count();
            return count;
        }

        #endregion
    }
}