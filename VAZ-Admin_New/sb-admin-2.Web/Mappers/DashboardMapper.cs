﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;

namespace sb_admin_2.Web.Mappers
{
    public class DashboardMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public DashBoard ConsolidatedNumbers()
        {
            return (from a in _db.SP_Dashboard_ConsolidatedNumbers() select new DashBoard { Users = a.Users.Value, Videos = a.Videos.Value, Adds = a.Adds.Value, Customers = a.Customers.Value }).SingleOrDefault();
        }

        public List<Point> ConsolidatedValues(string Type)
        {
            var data = (from a in _db.SP_Dashboard_ConsolidatedValues() where a.Type == Type select a).ToList();

            return (from a in data select new Point { x = a.XAxis, y = a.YAxis.Value }).ToList();
        }
    }
}