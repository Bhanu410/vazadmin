﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System.Globalization;

namespace sb_admin_2.Web.Mappers
{
    public class StoreMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public Store SingleStore(long Id)
        {
            return (from a in _db.tbl_Stores
                    where a.Id == Id
                    select new Store
                    {
                        Id = a.Id,
                        UniqueId = a.UniqueId,
                        Name = a.Name,
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        CreatedAt = a.CreatedAt,
                        IsActive = a.IsActive,
                        LastUpdatedAt = a.LastUpdatedAt,
                        ImagePath = a.ImagePath,
                        CustomerId = a.UniqueId
                    }).SingleOrDefault();
        }

        public List<Store> StoreList(string CustomerId)
        {
            return (from a in _db.tbl_Stores
                    where a.UniqueId == CustomerId
                    select new Store
                    {
                        Id = a.Id,
                        UniqueId = a.UniqueId,
                        Name = a.Name,
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        CreatedAt = a.CreatedAt,
                        IsActive = a.IsActive,
                        LastUpdatedAt = a.LastUpdatedAt,
                        ImagePath = a.ImagePath,
                        CustomerId = a.UniqueId
                    }).ToList();
        }

        public List<Store> StoreList(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;

            if (strFromDate != "")
            {
                FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (strToDate != "")
            {
                ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            return (from a in _db.tbl_Stores
                    join c in _db.tbl_Location on a.LocationId equals c.Id
                    join d in _db.tbl_SubLocation on a.SubLocationId equals d.Id
                    where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                            && (LocationId == 0 || a.LocationId == LocationId)
                            && (CustomerId == "" || a.UniqueId == CustomerId)
                    orderby a.Id descending
                    select new Store
                    {
                        Id = a.Id,
                        UniqueId = a.UniqueId,
                        Name = a.Name,
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        CreatedAt = a.CreatedAt,
                        IsActive = a.IsActive,
                        LastUpdatedAt = a.LastUpdatedAt,
                        LocationName = c.Name,
                        SubLocationName = d.Name,
                        ImagePath = a.ImagePath,
                        CustomerId = a.UniqueId
                    }).ToList();
        }

        public List<StoreRpt> StoreListRpt(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
        {

            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;

            if (strFromDate != "")
            {
                FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (strToDate != "")
            {
                ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            return (from a in _db.tbl_Stores
                    join c in _db.tbl_Location on a.LocationId equals c.Id
                    join d in _db.tbl_SubLocation on a.SubLocationId equals d.Id
                    where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                            && (LocationId == 0 || a.LocationId == LocationId)
                            && (CustomerId == "" || a.UniqueId == CustomerId)
                    orderby a.Id descending
                    select new StoreRpt
                    {
                        CustomerId = a.UniqueId,
                        Name = a.Name,
                        Location = c.Name,
                        SubLocation = d.Name,
                    }).ToList();
        }

        public List<Store> StoreList(long LocationId, long SubLocationId)
        {
            return (from a in _db.tbl_Stores
                    join c in _db.tbl_Location on a.LocationId equals c.Id
                    join d in _db.tbl_SubLocation on a.SubLocationId equals d.Id
                    where a.IsActive == 1 && a.LocationId == LocationId && a.SubLocationId == SubLocationId
                    orderby a.Id descending
                    select new Store
                    {
                        Id = a.Id,
                        UniqueId = a.UniqueId,
                        Name = a.Name,
                        LocationId = a.LocationId,
                        SubLocationId = a.SubLocationId,
                        CreatedAt = a.CreatedAt,
                        IsActive = a.IsActive,
                        LastUpdatedAt = a.LastUpdatedAt,
                        LocationName = c.Name,
                        SubLocationName = d.Name,
                        ImagePath = a.ImagePath,
                        CustomerId = a.UniqueId
                    }).ToList();
        }
   

        public int AddStore(Store sObj)
        {
            tbl_Stores tblStores = new tbl_Stores();
            tblStores.UniqueId = sObj.CustomerId;
            tblStores.Name = sObj.Name;
            tblStores.LocationId = sObj.LocationId;
            tblStores.SubLocationId = sObj.SubLocationId;
            tblStores.ImagePath = sObj.ImagePath;
            tblStores.CreatedAt = DateTime.Now;
            tblStores.IsActive = 1;

            _db.tbl_Stores.Add(tblStores);

           return _db.SaveChanges();
        }

        public int UpdateStore(Store sObj)
        {
            tbl_Stores tblStores = (from a in _db.tbl_Stores where a.Id == sObj.Id select a).SingleOrDefault();

            tblStores.Name = sObj.Name;
            tblStores.LocationId = sObj.LocationId;
            tblStores.SubLocationId = sObj.SubLocationId;
            tblStores.ImagePath = sObj.ImagePath;
            tblStores.UniqueId = sObj.CustomerId;

            return _db.SaveChanges();

        }

        public int DeleteStore(long Id)
        {
            tbl_Stores tblStores = (from a in _db.tbl_Stores where a.Id == Id select a).SingleOrDefault();

            tblStores.IsActive = 0;

            return _db.SaveChanges();
        }

       

  
    }
}