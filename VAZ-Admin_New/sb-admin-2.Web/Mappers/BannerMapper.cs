﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System.Globalization;

namespace sb_admin_2.Web.Mappers
{
  public class BannerMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    public List<Banner> GetBanners(string CustomerId)
    {

      var data = (from a in _db.tbl_BannerImages
                  join
                      b in _db.tbl_Location on a.LocationId equals b.Id
                  join
                  c in _db.tbl_Stores on a.StoreId equals c.Id
                  where a.IsActive == 1 && c.UniqueId == CustomerId
                  select new
                  {
                    Id = a.Id,
                    ImagePath = a.ImagePath,
                    LocationId = a.LocationId,
                    LocationName = b.Name,
                    CreatedDate = a.CreatedDate,
                    ExpireDate = a.ExpireDate,
                    SubLocationId = a.SubLocationId,
                    StoreId = a.StoreId,
                    StoreName = c.Name,
                    CustomerId = c.UniqueId
                  }).ToList();

      return (from a in data
              orderby a.Id descending
              select new Banner
              {
                Id = a.Id,
                ImagePath = a.ImagePath,
                LocationId = a.LocationId,
                LocationName = a.LocationName,
                strCreatedDate = a.CreatedDate.Value.ToString("dd MMM yyyy"),
                strExpiryDate = a.ExpireDate.Value.ToString("dd MMM yyyy"),
                //Days = CalculateDaysLeft(a.ExpireDate.Value),
                //Status = (a.ExpireDate.Value - DateTime.Now).TotalDays > 0 ? "Open" : "Closed",
                Status = a.ExpireDate >= DateTime.Now ? "Open" : "Closed",
                SubLocationId = a.SubLocationId,
                StoreId = a.StoreId,
                StoreName = a.StoreName,
                CustomerId = a.CustomerId
              }).ToList();


    }

    public int PostBanner(Banner Obj)
    {
      tbl_BannerImages tblBanners = new tbl_BannerImages();

      tblBanners.ImagePath = Obj.ImagePath;
      tblBanners.IsActive = 1;

      tblBanners.LocationId = Obj.LocationId;
      tblBanners.CreatedDate = DateTime.Now;
      // tblBanners.ExpireDate = DateTime.Now.AddDays(Obj.Days);
      tblBanners.ExpireDate = DateTime.ParseExact(Obj.strExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      tblBanners.SubLocationId = Obj.SubLocationId;
      tblBanners.StoreId = Obj.StoreId;

      _db.tbl_BannerImages.Add(tblBanners);
      int status = _db.SaveChanges();

      BannerLocationMapping(Obj, tblBanners.Id);
      BannerSubLocationMapping(Obj, tblBanners.Id);

      return status;

    }

    public int UpdateBanner(Banner Obj)
    {
      tbl_BannerImages tblBanners = (from a in _db.tbl_BannerImages where a.Id == Obj.Id select a).SingleOrDefault();

      tblBanners.ImagePath = Obj.ImagePath;
      tblBanners.IsActive = 1;

      tblBanners.LocationId = Obj.LocationId;

      //tblBanners.ExpireDate = tblBanners.CreatedDate.Value.AddDays(Obj.Days);
      tblBanners.ExpireDate = DateTime.ParseExact(Obj.strExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;
      tblBanners.SubLocationId = Obj.SubLocationId;
      tblBanners.StoreId = Obj.StoreId;

      BannerLocationMapping(Obj, tblBanners.Id);
      BannerSubLocationMapping(Obj, tblBanners.Id);

      return _db.SaveChanges();

    }

    public Banner SingleBanner(long Id)
    {
      var data = (from a in _db.tbl_BannerImages
                  where a.Id == Id
                  select new
                  {
                    Id = a.Id,
                    ImagePath = a.ImagePath,
                    LocationId = a.LocationId,
                    CreatedDate = a.CreatedDate,
                    ExpireDate = a.ExpireDate,
                    SubLocationId = a.SubLocationId,
                    StoreId = a.StoreId

                  }).ToList();

      return (from a in data
              select new Banner
              {
                Id = a.Id,
                ImagePath = a.ImagePath,
                strCreatedDate = a.CreatedDate.Value.ToString("dd MMM yyyy"),
                LocationId = a.LocationId,
                strExpiryDate = a.ExpireDate.Value.ToString("dd/MM/yyyy"),
                Status = a.ExpireDate >= DateTime.Now ? "Open" : "Closed",
                SubLocationId = a.SubLocationId,
                StoreId = a.StoreId
              }).SingleOrDefault();
    }

    public int DeleteBanner(long BannerId)
    {
      tbl_BannerImages rec = (from a in _db.tbl_BannerImages where a.Id == BannerId select a).SingleOrDefault();
      _db.tbl_BannerImages.Remove(rec);

      return _db.SaveChanges();
    }

    private double CalculateDaysLeft(DateTime ExipreDate)
    {
      return (ExipreDate - DateTime.Now).TotalDays < 0 ? 0 : Math.Round((ExipreDate - DateTime.Now).TotalDays, 0);
    }


    #region Reports

    public List<Banner> GetBanners(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_BannerImages
                  
                     
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedDate >= FromDate && a.CreatedDate.Value <= ToDate))
                          && (LocationId == 0 || a.LocationId == LocationId)
                          && (Status == "" || (Status == "Open" ? a.ExpireDate >= DateTime.Now : a.ExpireDate <= DateTime.Now))
                  select new
                  {
                    Id = a.Id,
                    ImagePath = a.ImagePath,
                    LocationId = a.LocationId,
                   
                    CreatedDate = a.CreatedDate,
                    ExpireDate = a.ExpireDate,
                    SubLocationId = a.SubLocationId,
                    StoreId = a.StoreId
                  }).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new Banner
                         {
                           Id = a.Id,
                           ImagePath = a.ImagePath,
                           LocationId = a.LocationId,
                          
                           strCreatedDate = a.CreatedDate.Value.ToString("dd MMM yyyy"),
                           strExpiryDate = a.ExpireDate.Value.ToString("dd MMM yyyy"),
                           Status = a.ExpireDate >= DateTime.Now ? "Open" : "Closed",
                           SubLocationId = a.SubLocationId,
                           StoreId = a.StoreId,
                           StoreName = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           CustomerName = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();



      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    public List<BannerRpt> GetBannersRpt(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_BannerImages
                  join
                      b in _db.tbl_Location on a.LocationId equals b.Id
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedDate >= FromDate && a.CreatedDate.Value <= ToDate))
                          && (LocationId == 0 || a.LocationId == LocationId)
                          && (Status == "" || (Status == "Open" ? a.ExpireDate >= DateTime.Now : a.ExpireDate <= DateTime.Now))
                  select new
                  {
                    Id = a.Id,
                    ImagePath = a.ImagePath,
                    LocationId = a.LocationId,
                    LocationName = b.Name,
                    CreatedDate = a.CreatedDate,
                    ExpireDate = a.ExpireDate,
                    SubLocationId = a.SubLocationId,
                    StoreId = a.StoreId
                  }).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new BannerRpt
                         {
                           Location = a.LocationName,
                           DateofUpload = a.CreatedDate.Value.ToString("dd MMM yyyy"),
                           ExpiryDate = a.ExpireDate.Value.ToString("dd MMM yyyy"),
                           Status = a.ExpireDate >= DateTime.Now ? "Open" : "Closed",
                           //Store = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           Customer = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();

      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;


    }

    public List<CustomerActivityReport> GetBannersCustom(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_BannerImages
                  join
                      b in _db.tbl_Location on a.LocationId equals b.Id
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedDate >= FromDate && a.CreatedDate.Value <= ToDate))
                          && (LocationId == 0 || a.LocationId == LocationId)
                          && (Status == "" || (Status == "Open" ? a.ExpireDate >= DateTime.Now : a.ExpireDate <= DateTime.Now))
                  select new
                  {
                    Id = a.Id,
                    ImagePath = a.ImagePath,
                    LocationId = a.LocationId,
                    LocationName = b.Name,
                    CreatedDate = a.CreatedDate,
                    ExpireDate = a.ExpireDate,
                    SubLocationId = a.SubLocationId,
                    StoreId = a.StoreId
                  }).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new CustomerActivityReport
                         {
                           Type = "Banner",
                           Location = a.LocationName,
                           DateofUpload = a.CreatedDate.Value.ToString("dd MMM yyyy"),
                           ExpiryDate = a.ExpireDate.Value.ToString("dd MMM yyyy"),
                           Status = a.ExpireDate >= DateTime.Now ? "Open" : "Closed",
                           Store = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           Customer = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();



      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    #endregion


    public void BannerLocationMapping(Banner a, long BannerId)
    {
      try
      {
        var tbl = (from x in _db.tbl_Banner_Location_Mapping where x.BannerId == BannerId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Banner_Location_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Banner_Location_Mapping tblObj = new tbl_Banner_Location_Mapping();

        string[] strArray = (a.BannerLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.BannerId = BannerId;
          tblObj.LocationId = long.Parse(item);
          _db.tbl_Banner_Location_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }
      

    }

    public void BannerSubLocationMapping(Banner a, long BannerId)
    {
      try
      {
        LocationMapper _locationMapper = new LocationMapper();

        var tbl = (from x in _db.tbl_Banner_Sublocation_Mapping where x.BannerId == BannerId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Banner_Sublocation_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Banner_Sublocation_Mapping tblObj = new tbl_Banner_Sublocation_Mapping();


        string[] strArray = (a.BannerSubLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.BannerId = BannerId;
          tblObj.SubLocationId = long.Parse(item);
          tblObj.LocationId = _locationMapper.SingleSubLocation(long.Parse(item)).LocationId;
          _db.tbl_Banner_Sublocation_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }

    }


    public List<Location> GetBannerLocations(long BannerId)
    {
      
      return ( from c in _db.tbl_Location
              join p in (from a in _db.tbl_Banner_Location_Mapping where a.BannerId == BannerId select a) on c.Id equals p.LocationId into ps
              from p in ps.DefaultIfEmpty()
              select new Location { Id = c.Id, Name = c.Name, Flag = p.BannerId == null ? false : true }).ToList();



    }

    public List<SubLocation> GetBannerSubLocations(List<long> LocationId, long BannerId)
    {

      return (from c in _db.tbl_SubLocation
              join p in (from a in _db.tbl_Banner_Sublocation_Mapping where a.BannerId == BannerId select a) on c.Id equals p.SubLocationId into ps
              from p in ps.DefaultIfEmpty()
              where LocationId.Contains(c.LocationId.Value)
              select new SubLocation { Id = c.Id, Name = c.Name, Flag = p.BannerId == null ? false : true }).ToList();



    }

  }
}