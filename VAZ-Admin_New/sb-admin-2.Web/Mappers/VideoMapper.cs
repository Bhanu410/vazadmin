﻿using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Mappers
{
  public class VideoMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    public Video SingleVideo(long Id)
    {
      var data = (from a in _db.tbl_Videos
                  where a.Id == Id
                  select a).ToList();

      return (from a in data
              select new Video
              {
                Id = a.Id,
                Name = a.Name,
                Description = a.Description,
                VideoPath = a.VideoPath,
                RewardPoints = a.RewardPoints,
                LocationId = a.LocationId,
                Time = a.Time,
                ViewsLimit = a.ViewsLimit,
                strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                SubLocationId = a.SubLocationId,
                StoreId = a.StoreId
              }).SingleOrDefault();
    }


    public List<Video> GetVideos(string CustomerId)
    {
      var data = (from a in _db.tbl_Videos
                  join
               b in _db.tbl_Location on a.LocationId equals b.Id
                  join
                    c in _db.tbl_Stores on a.StoreId equals c.Id
                  where a.IsActive == 1 && c.UniqueId == CustomerId
                  select a).ToList();

      return (from a in data
              orderby a.Id descending
              select new Video
              {
                Id = a.Id,
                Name = a.Name,
                Description = a.Description,
                VideoPath = a.VideoPath,
                RewardPoints = a.RewardPoints,
                LocationId = a.LocationId,
                LocationName = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).First() + "",
                Time = a.Time,
                ViewsLimit = a.ViewsLimit,
                strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                SubLocationId = a.SubLocationId,
                StoreId = a.StoreId
              }).ToList();
    }

    public int PostVideo(Video Obj)
    {
      tbl_Videos tblVideo = new tbl_Videos();

      string uId = Guid.NewGuid().ToString();

      tblVideo.Name = Obj.Name;
      tblVideo.VideoPath = Obj.VideoPath;
      tblVideo.Description = Obj.Description;
      tblVideo.RewardPoints = Obj.RewardPoints;
      tblVideo.LocationId = Obj.LocationId;
      tblVideo.LocationName = Obj.LocationName;
      tblVideo.CreatedAt = CurrentIndianDate();
      tblVideo.IsActive = 1;
      tblVideo.Time = Obj.Time;
      tblVideo.ViewsLimit = Obj.ViewsLimit;
      tblVideo.StoreId = Obj.StoreId;
      tblVideo.SubLocationId = Obj.SubLocationId;
      tblVideo.UniqueId = uId;
      tblVideo.ReassignDate = CurrentIndianDate();

      _db.tbl_Videos.Add(tblVideo);

      int status = _db.SaveChanges();

      VideoLocationMapping(Obj, tblVideo.Id);
      VideoSubLocationMapping(Obj, tblVideo.Id);

      return status;

    }

    public int UpdateVideo(Video Obj)
    {
      tbl_Videos tblVideo = (from a in _db.tbl_Videos where a.Id == Obj.Id select a).SingleOrDefault();

      tblVideo.Name = Obj.Name;
      tblVideo.VideoPath = Obj.VideoPath;
      tblVideo.Description = Obj.Description;
      tblVideo.RewardPoints = Obj.RewardPoints;
      tblVideo.LocationId = Obj.LocationId;
      tblVideo.LocationName = Obj.LocationName;

      tblVideo.IsActive = 1;
      tblVideo.Time = Obj.Time;
      tblVideo.ViewsLimit = Obj.ViewsLimit;
      tblVideo.StoreId = Obj.StoreId;
      tblVideo.SubLocationId = Obj.SubLocationId;


      int status = _db.SaveChanges();

      VideoLocationMapping(Obj, tblVideo.Id);
      VideoSubLocationMapping(Obj, tblVideo.Id);

      return status;

    }

    public int DeleteVideo(long Id)
    {
      tbl_Videos rec = (from a in _db.tbl_Videos where a.Id == Id select a).SingleOrDefault();
      rec.IsActive = 0;

      _db.tbl_UserActivity.RemoveRange(_db.tbl_UserActivity.Where(x => x.Type == "VIDEO" && x.AddId == rec.Id && x.IsViewed == 0 && x.IsLocked != 1));

      return _db.SaveChanges();
    }


    public int Reassign(long Id)
    {
      tbl_Videos tblVideos = new tbl_Videos();
      tbl_Videos Obj = (from a in _db.tbl_Videos where a.Id == Id select a).SingleOrDefault();


      tblVideos.Name = Obj.Name;
      tblVideos.VideoPath = Obj.VideoPath;
      tblVideos.Description = Obj.Description;
      tblVideos.RewardPoints = Obj.RewardPoints;
      tblVideos.LocationId = Obj.LocationId;
      tblVideos.LocationName = Obj.LocationName;
      tblVideos.CreatedAt = Obj.CreatedAt;
      tblVideos.IsActive = 1;
      tblVideos.Time = Obj.Time;
      tblVideos.ViewsLimit = Obj.ViewsLimit;
      tblVideos.StoreId = Obj.StoreId;
      tblVideos.SubLocationId = Obj.SubLocationId;
      tblVideos.UniqueId = Obj.UniqueId;
      tblVideos.ReassignDate = CurrentIndianDate();

      _db.tbl_Videos.Add(tblVideos);

      //Inactivate Old Video
      Obj.IsActive = 0;

      int i = _db.SaveChanges();

      _db.tbl_UserActivity.RemoveRange(_db.tbl_UserActivity.Where(x => x.Type == "VIDEO" && x.AddId == Obj.Id && x.IsViewed == 0 && x.IsLocked != 1));


      var tblLoc = (from a in _db.tbl_Videos_Location_Mapping where a.VideoId == Id select a).ToList();

      foreach (var Item in tblLoc)
      {
        Item.VideoId = tblVideos.Id;
       
      }

      var tblSubLoc = (from a in _db.tbl_Videos_Sublocation_Mapping where a.VideoId == Id select a).ToList();

      foreach (var Item in tblSubLoc)
      {
        Item.VideoId = tblVideos.Id;
       
      }

      _db.SaveChanges();

      return i;
    }

    public List<Video> GetVideos(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_Videos
                 
                 
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                      && (LocationId == 0 || a.LocationId == LocationId)
                      && (Status == "" || (Status == "Open" ? a.ViewsLimit > 0 : a.ViewsLimit == 0))

                  select a).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new Video
                         {
                           Id = a.Id,
                           Name = a.Name,
                           Description = a.Description,
                           VideoPath = a.VideoPath,
                           RewardPoints = a.RewardPoints,
                           LocationId = a.LocationId,
                           LocationName = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).FirstOrDefault() + "",
                           Time = a.Time,
                           ViewsLimit = a.ViewsLimit,
                           strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                           Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                           SubLocationId = a.SubLocationId,
                           StoreId = a.StoreId,
                           StoreName = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           CustomerName = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();

      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    public List<RptAdvertisement> GetVideosReport(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_Videos
                  join
                  b in _db.tbl_Location on a.LocationId equals b.Id
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                      && (LocationId == 0 || a.LocationId == LocationId)
                      && (Status == "" || (Status == "Open" ? a.ViewsLimit > 0 : a.ViewsLimit == 0))

                  select a).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new RptAdvertisement
                         {

                           Title = a.Name,
                           Description = a.Description,
                           RewardPoints = a.RewardPoints,
                           Location = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).First() + "",
                           Time = a.Time,
                           ViewsLeft = a.ViewsLimit,
                           DateofUpload = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                           Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                           //Store = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           Customer = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();

      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    public List<CustomerActivityReport> GetVideosCustom(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string CustomerId = "")
    {
      DateTime FromDate = DateTime.Now;
      DateTime ToDate = DateTime.Now;

      if (strFromDate != "")
      {
        FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      if (strToDate != "")
      {
        ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      }

      var data = (from a in _db.tbl_Videos
                  join
                  b in _db.tbl_Location on a.LocationId equals b.Id
                  where a.IsActive == 1 && (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                      && (LocationId == 0 || a.LocationId == LocationId)
                      && (Status == "" || (Status == "Open" ? a.ViewsLimit > 0 : a.ViewsLimit == 0))

                  select a).ToList();

      var FilterdData = (from a in data
                         orderby a.Id descending
                         select new CustomerActivityReport
                         {
                           Type = "Video",
                           Title = a.Name,
                           Description = a.Description,
                           RewardPoints = a.RewardPoints.Value,
                           Location = (from b in _db.tbl_Location where b.Id == a.LocationId select b.Name).First() + "",
                           ViewsLeft = a.ViewsLimit.Value,
                           DateofUpload = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                           Status = a.ViewsLimit > 0 ? "Open" : "Closed",
                           Store = (a.StoreId != null ? (from b in _db.tbl_Stores where b.Id == a.StoreId select b.Name).FirstOrDefault() : ""),
                           Customer = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.ComapanyName).FirstOrDefault() : ""),
                           CustomerId = (a.StoreId != null ? (from c in _db.tbl_Customers where c.Id == a.StoreId select c.CustomerId).FirstOrDefault() : "")
                         }).ToList();

      if (CustomerId != "")
      {
        FilterdData = (from a in FilterdData where a.CustomerId == CustomerId select a).ToList();
      }

      return FilterdData;
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }


    public void VideoLocationMapping(Video a, long VideoId)
    {
      try
      {
        var tbl = (from x in _db.tbl_Videos_Location_Mapping where x.VideoId == VideoId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Videos_Location_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Videos_Location_Mapping tblObj = new tbl_Videos_Location_Mapping();

        string[] strArray = (a.VideoLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.VideoId = VideoId;
          tblObj.LocationId = long.Parse(item);
          _db.tbl_Videos_Location_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
     catch
      {

      }

    }

    public void VideoSubLocationMapping(Video a, long VideoId)
    {
      try
      {
        LocationMapper _locationMapper = new LocationMapper();

        var tbl = (from x in _db.tbl_Videos_Sublocation_Mapping where x.VideoId == VideoId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Videos_Sublocation_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Videos_Sublocation_Mapping tblObj = new tbl_Videos_Sublocation_Mapping();


        string[] strArray = (a.VideoSubLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.VideoId = VideoId;
          tblObj.SubLocationId = long.Parse(item);
          tblObj.LocationId = _locationMapper.SingleSubLocation(long.Parse(item)).LocationId;
          _db.tbl_Videos_Sublocation_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }

    }


    public List<Location> GetVideoLocations(long VideoId)
    {

      return (from c in _db.tbl_Location
              join p in (from a in _db.tbl_Videos_Location_Mapping where a.VideoId == VideoId select a) on c.Id equals p.LocationId into ps
              from p in ps.DefaultIfEmpty()
              select new Location { Id = c.Id, Name = c.Name, Flag = p.VideoId == null ? false : true }).ToList();



    }

    public List<SubLocation> GetVideoSubLocations(List<long> LocationId, long VideoId)
    {

      return (from c in _db.tbl_SubLocation
              join p in (from a in _db.tbl_Videos_Sublocation_Mapping where a.VideoId == VideoId select a) on c.Id equals p.SubLocationId into ps
              from p in ps.DefaultIfEmpty()
              where LocationId.Contains(c.LocationId.Value)
              select new SubLocation { Id = c.Id, Name = c.Name, Flag = p.VideoId == null ? false : true }).ToList();



    }

  }
}