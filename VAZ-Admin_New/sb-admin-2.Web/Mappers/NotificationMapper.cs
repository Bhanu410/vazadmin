﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;

namespace sb_admin_2.Web.Mappers
{
    public class NotificationMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public long RecordNotificationData(Notification nObj)
        {
            tbl_NotificationOutbox tblObj = new tbl_NotificationOutbox();

            tblObj.UserId = nObj.UserId;
            tblObj.Title = nObj.Title;
            tblObj.Body = nObj.Message;
            tblObj.SentTo = nObj.SentTo;
            tblObj.Mobile = nObj.Mobile;
            tblObj.CreatedAt = nObj.CreatedAt;

            _db.tbl_NotificationOutbox.Add(tblObj);

            _db.SaveChanges();

            return tblObj.Id;

        }

        public List<NotificationOutbox> GetNotificationData()
        {
            return (from a in _db.tbl_NotificationOutbox
                    orderby a.Id descending
                    select new NotificationOutbox
                    {
                        SentTo = a.SentTo,
                        Mobile = a.Mobile,
                        Title = a.Title,
                        Message = a.Body
                    }).ToList();
        }
    }
}