﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;

namespace sb_admin_2.Web.Mappers
{
    public class LoginMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public Login AuthenticateUser(Login loginObj)
        {
           return (from a in _db.tbl_AdminUsers where a.UserName == loginObj.UserName && a.Password == loginObj.Password select new Login {UserName = a.UserName}).SingleOrDefault();
        }

    }
}