﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;

namespace sb_admin_2.Web.Mappers
{
    public class LocationMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public Location SingleLocation(long Id)
        {
            return (from a in _db.tbl_Location where a.Id == Id select new Location { Id = a.Id, Name = a.Name }).SingleOrDefault();
        }

        public List<Location> GetLocations()
        {
            return (from a in _db.tbl_Location where a.IsActive == 1 orderby a.Id descending select new Location { Id = a.Id, Name = a.Name }).ToList();
        }

        public List<Location> GetLocationsByName(string LocationName)
        {
            return (from a in _db.tbl_Location where a.IsActive == 1 && a.Name == LocationName orderby a.Id descending select new Location { Id = a.Id, Name = a.Name }).ToList();
        }
      

        public int PostLocation(Location Obj)
        {
            tbl_Location tblLocation = new tbl_Location();

            tblLocation.Name = Obj.Name;
            tblLocation.IsActive = 1;

            _db.tbl_Location.Add(tblLocation);
            return _db.SaveChanges();

        }

        public int UpdateLocation(Location Obj)
        {
            tbl_Location tblLocation = (from a in _db.tbl_Location where a.Id == Obj.Id select a).SingleOrDefault();

            tblLocation.Name = Obj.Name;
            tblLocation.IsActive = 1;

            return _db.SaveChanges();

        }

        public int DeleteLocation(long LocationId)
        {
            tbl_Location rec = (from a in _db.tbl_Location where a.Id == LocationId select a).SingleOrDefault();
            _db.tbl_Location.Remove(rec);

            return _db.SaveChanges();
        }

        public SubLocation SingleSubLocation(long Id)
        {
            return (from a in _db.tbl_SubLocation
                    join b in _db.tbl_Location on a.LocationId equals b.Id
                    where a.Id == Id
                    select new SubLocation
                    {
                        Id = a.Id,
                        Name = a.Name,
                        LocationName = b.Name,
                        LocationId = a.LocationId.Value
                    }).SingleOrDefault();
        }

        public List<SubLocation> GetSubLocations()
        {
            return (from a in _db.tbl_SubLocation
                    join b in _db.tbl_Location on a.LocationId equals b.Id
                    where a.IsActive == 1
                    orderby a.Id descending
                    select new SubLocation
                    {
                        Id = a.Id,
                        Name = a.Name,
                        LocationName = b.Name
                    }).ToList();
        }

        public List<SubLocation> GetSubLocationsByName(string SubLocationName, long LocationId)
        {
            return (from a in _db.tbl_SubLocation
                    where a.IsActive == 1  && a.Name == SubLocationName && a.LocationId == LocationId
                   
                    orderby a.Id descending
                    select new SubLocation
                    {
                        Id = a.Id,
                        Name = a.Name
                    }).ToList();
        }

        public List<SubLocation> GetSubLocationsByLocation(long LocationId)
        {
            return (from a in _db.tbl_SubLocation
                    join b in _db.tbl_Location on a.LocationId equals b.Id
                    where a.LocationId == LocationId
                    select new SubLocation
                    {
                        Id = a.Id,
                        Name = a.Name,
                        LocationName = b.Name
                    }).ToList();
        }

    public List<SubLocation> GetSubLocationsByMultipleLocation(List<long> LocationId)
    {
      return (from a in _db.tbl_SubLocation
              join b in _db.tbl_Location on a.LocationId equals b.Id
              where LocationId.Contains(a.LocationId.Value)
              select new SubLocation
              {
                Id = a.Id,
                Name = a.Name,
                LocationName = b.Name
              }).ToList();
    }

    public int PostSubLocation(SubLocation Obj)
        {
            tbl_SubLocation tblSubLocation = new tbl_SubLocation();

            tblSubLocation.Name = Obj.Name;
            tblSubLocation.LocationId = Obj.LocationId;
            tblSubLocation.IsActive = 1;

            _db.tbl_SubLocation.Add(tblSubLocation);
            return _db.SaveChanges();

        }

        public int UpdateSubLocation(SubLocation Obj)
        {
            tbl_SubLocation tblSubLocation = (from a in _db.tbl_SubLocation where a.Id == Obj.Id select a).SingleOrDefault();

            tblSubLocation.Name = Obj.Name;
            tblSubLocation.LocationId = Obj.LocationId;
            tblSubLocation.IsActive = 1;

            return _db.SaveChanges();

        }

        public int DeleteSubLocation(long SubLocationId)
        {
            tbl_SubLocation rec = (from a in _db.tbl_SubLocation where a.Id == SubLocationId select a).SingleOrDefault();
            _db.tbl_SubLocation.Remove(rec);

            return _db.SaveChanges();
        }

    }
}