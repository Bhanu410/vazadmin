﻿using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace sb_admin_2.Web.Mappers
{
  public class MagazineMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    public List<Magazine> GetMagazineList()
    {
      var data = (from a in _db.tbl_Magazines
                  
                  where a.IsActive == true
                  select new
                  {
                    a.Id,
                    a.Title,
                    a.Description,
                    a.ExpiryDate,
                    a.LocationId,
                    a.CreatedAt,
                    a.CreatedBy,
                    a.LastUpdatedAt,
                    a.LastUpdatedBy,
                    a.IsActive,
                   
                    a.ImagePath,
                    a.MagazineDate
                  }).ToList();

      return (from a in data
              orderby a.Id descending
              select new Magazine
              {
                Id = a.Id,
                Title = a.Title,
                Description = a.Description,
                strExpiryDate = a.ExpiryDate.Value.ToString("dd/MM/yyyy"),
                strMagazineDate = a.MagazineDate == null ? null : a.MagazineDate.Value.ToString("dd/MM/yyyy"),
                strCreatedAt = a.CreatedAt.Value.ToString("dd/MM/yyyy"),

                LocationId = a.LocationId,
                IsActive = a.IsActive,
                Status = a.ExpiryDate.Value >= DateTime.Now ? "Open" : "Closed",
                ImagePath = a.ImagePath
              }).ToList();
    }

    public Magazine GetSingleMagazineObj(long uId)
    {
      var data = (from a in _db.tbl_Magazines
                 
                  where a.Id == uId
                  select new
                  {
                    a.Id,
                    a.Title,
                    a.Description,
                    a.ExpiryDate,
                    a.LocationId,
                    a.CreatedAt,
                    a.CreatedBy,
                    a.LastUpdatedAt,
                    a.LastUpdatedBy,
                    a.IsActive,
                   
                    a.ImagePath,
                    a.MagazineDate
                  }).ToList();

      return (from a in data
              orderby a.Id descending
              select new Magazine
              {
                Id = a.Id,
                Title = a.Title,
                Description = a.Description,
                strExpiryDate = a.ExpiryDate.Value.ToString("dd/MM/yyyy"),
                strMagazineDate = a.MagazineDate == null ? null : a.MagazineDate.Value.ToString("dd/MM/yyyy"),
                strCreatedAt = a.CreatedAt.Value.ToString("dd/MM/yyyy hh:mm"),
             
                LocationId = a.LocationId,
                IsActive = a.IsActive,
                Status = a.ExpiryDate.Value >= DateTime.Now ? "Open" : "Closed",
                ImagePath = a.ImagePath
              }).SingleOrDefault();
    }

    public long PostMagazine(Magazine nObj)
    {
      tbl_Magazines tblObj = new tbl_Magazines();

      tblObj.Title = nObj.Title;
      tblObj.Description = nObj.Description;
      tblObj.ExpiryDate = DateTime.ParseExact(nObj.strExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      tblObj.LocationId = nObj.LocationId;
      tblObj.IsActive = true;
      tblObj.CreatedAt = CurrentIndianDate();
      tblObj.ImagePath = nObj.ImagePath;
      tblObj.MagazineDate = DateTime.ParseExact(nObj.strMagazineDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

      _db.tbl_Magazines.Add(tblObj);
      _db.SaveChanges();

      MagazineLocationMapping(nObj, tblObj.Id);
   
      return tblObj.Id;

    }

    public int UpdateMagazine(Magazine nObj)
    {
      tbl_Magazines tblObj = (from a in _db.tbl_Magazines where a.Id == nObj.Id select a).SingleOrDefault();

      tblObj.Title = nObj.Title;
      tblObj.Description = nObj.Description;
      tblObj.ExpiryDate = DateTime.ParseExact(nObj.strExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
      tblObj.LocationId = nObj.LocationId;
      tblObj.LastUpdatedAt = CurrentIndianDate();
      tblObj.ImagePath = nObj.ImagePath;
      tblObj.MagazineDate = DateTime.ParseExact(nObj.strMagazineDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

      int status = _db.SaveChanges();

      MagazineLocationMapping(nObj, tblObj.Id);
    
      return status;
    }

    public int DeleteMagazine(long uId)
    {
      tbl_Magazines rec = (from a in _db.tbl_Magazines where a.Id == uId select a).SingleOrDefault();

      _db.tbl_Magazines.Remove(rec);
      return _db.SaveChanges();
    }

    public List<MagazineImages> GetMagazineImages(long MagazineId)
    {
      return (from a in _db.tbl_MagazineImages
              where a.MagazineId == MagazineId
              select new MagazineImages
              {
                Id = a.Id,
                MagazineId = a.MagazineId,
                ImagePath = a.ImagePath
              }).ToList();

    }

    public int UploadMultipleMagazineImages(MagazineImages iObj)
    {
      tbl_MagazineImages tbl = new tbl_MagazineImages();
      tbl.MagazineId = iObj.MagazineId;
      tbl.ImagePath = iObj.ImagePath;
      _db.tbl_MagazineImages.Add(tbl);
      return _db.SaveChanges();
    }

    public int DeleteMagazineImage(long Id)
    {
      tbl_MagazineImages rec = (from a in _db.tbl_MagazineImages where a.Id == Id select a).SingleOrDefault();
      _db.tbl_MagazineImages.Remove(rec);
      return _db.SaveChanges();
    }

    public int MagazineImageCount(long MagazineId)
    {
      int count = (from a in _db.tbl_MagazineImages where a.MagazineId == MagazineId select a).Count();
      return count;
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }

    public void MagazineLocationMapping(Magazine a, long MagazineId)
    {
      try
      {
        var tbl = (from x in _db.tbl_Magazine_Location_Mapping where x.MagazineId == MagazineId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Magazine_Location_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Magazine_Location_Mapping tblObj = new tbl_Magazine_Location_Mapping();

        string[] strArray = (a.MagazineLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.MagazineId = MagazineId;
          tblObj.LocationId = long.Parse(item);
          _db.tbl_Magazine_Location_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }

    }

    public void MagazineSubLocationMapping(Magazine a, long MagazineId)
    {
      try
      {
        LocationMapper _locationMapper = new LocationMapper();

        var tbl = (from x in _db.tbl_Magazine_Sublocation_Mapping where x.MagazineId == MagazineId select x).ToList();

        foreach (var rec in tbl)
        {
          _db.tbl_Magazine_Sublocation_Mapping.Remove(rec);
          _db.SaveChanges();
        }


        tbl_Magazine_Sublocation_Mapping tblObj = new tbl_Magazine_Sublocation_Mapping();


        string[] strArray = (a.MagazineSubLocation + "").Trim(',').Split(',');

        foreach (var item in strArray)
        {
          tblObj.MagazineId = MagazineId;
          tblObj.SubLocationId = long.Parse(item);
          tblObj.LocationId = _locationMapper.SingleSubLocation(long.Parse(item)).LocationId;
          _db.tbl_Magazine_Sublocation_Mapping.Add(tblObj);
          _db.SaveChanges();
        }
      }
      catch
      {

      }

    }


    public List<Location> GetMagazineLocations(long MagazineId)
    {

      return (from c in _db.tbl_Location
              join p in (from a in _db.tbl_Magazine_Location_Mapping where a.MagazineId == MagazineId select a) on c.Id equals p.LocationId into ps
              from p in ps.DefaultIfEmpty()
              select new Location { Id = c.Id, Name = c.Name, Flag = p.MagazineId == null ? false : true }).ToList();



    }

    public List<SubLocation> GetMagazineSubLocations(List<long> LocationId, long MagazineId)
    {

      return (from c in _db.tbl_SubLocation
              join p in (from a in _db.tbl_Magazine_Sublocation_Mapping where a.MagazineId == MagazineId select a) on c.Id equals p.SubLocationId into ps
              from p in ps.DefaultIfEmpty()
              where LocationId.Contains(c.LocationId.Value)
              select new SubLocation { Id = c.Id, Name = c.Name, Flag = p.MagazineId == null ? false : true }).ToList();



    }


  }
}