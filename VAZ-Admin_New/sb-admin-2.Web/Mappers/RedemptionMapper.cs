﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;

namespace sb_admin_2.Web.Mappers
{
  public class RedemptionMapper
  {
    ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

    UserMapper _userMapper = new UserMapper();

    public List<Redemption> getRequests(bool isPending)
    {
      var data = (from a in _db.tbl_RedemptionRequests join b in _db.tbl_Users on a.UserId equals b.Id where a.IsPending == isPending select new
      {
        Id = a.Id,
        AccountHolderName = a.AccountHolderName,
        AccountNumber = a.AccountNumber,
        Amount = a.Amount,
        BankName = a.BankName,
        IFSC = a.IFSC,
        Mobile = a.Mobile,
        IsPending = a.IsPending,
        Points = a.Points,
        UserId = a.UserId,
        CreatedAt = a.CreatedAt,
        ApprovedAt = a.ApprovedAt,
        UUId = b.UserId
      }).ToList();

      return (from a in data
              orderby a.Id descending
              select new Redemption
              {
                Id = a.Id,
                AccountHolderName = a.AccountHolderName,
                AccountNumber = a.AccountNumber,
                Amount = a.Amount,
                BankName = a.BankName,
                IFSC = a.IFSC,
                Mobile = a.Mobile,
                IsPending = a.IsPending,
                Points = a.Points,
                UserId = a.UserId,
                UUId = a.UUId,
                strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy HH:mm"),
                strApprovedAt = a.ApprovedAt == null ? null : a.ApprovedAt.Value.ToString("dd MMM yyyy HH:mm")
              }).ToList();
    }

    public int ApproveRequest(long RequestId)
    {
      tbl_RedemptionRequests tblObj = (from a in _db.tbl_RedemptionRequests where a.Id == RequestId select a).SingleOrDefault();
      tblObj.IsPending = false;
      tblObj.ApprovedAt = CurrentIndianDate();

      return _db.SaveChanges();
    }

    public User getUserByRequestId(long RequestId)
    {
      var data = (from a in _db.tbl_RedemptionRequests where a.Id == RequestId select a).SingleOrDefault();

      return _userMapper.User(data.UserId.Value);
    }

    public Redemption RequestDetail(long RequestId)
    {
      var data = (from a in _db.tbl_RedemptionRequests where a.Id == RequestId select a).ToList();

      return (from a in data
              orderby a.Id descending
              select new Redemption
              {
                Id = a.Id,
                AccountHolderName = a.AccountHolderName,
                AccountNumber = a.AccountNumber,
                Amount = a.Amount,
                BankName = a.BankName,
                IFSC = a.IFSC,
                Mobile = a.Mobile,
                IsPending = a.IsPending,
                Points = a.Points,
                UserId = a.UserId,
                strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy HH:mm")
              }).SingleOrDefault();
    }


    public PointsDefinition getDefinition()
    {
      return (from a in _db.tbl_PointsDefinition select new PointsDefinition { Points = a.Points, Rupees = a.Rupees }).First();
    }

    public int UpdateDefinition(PointsDefinition pObj)
    {
      tbl_PointsDefinition tblObj = (from a in _db.tbl_PointsDefinition select a).First();

      tblObj.Rupees = pObj.Rupees;
      tblObj.Points = pObj.Points;

      return _db.SaveChanges();
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }
  }
}