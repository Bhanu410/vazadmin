﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;

namespace sb_admin_2.Web.Mappers
{
    public class GlobalValuesMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public List<GlobalValues> Globals()
        {
            return (from a in _db.tbl_Globals select new GlobalValues { Id = a.Id, Name = a.Name, Value = a.Value }).ToList();
        }
    }
}