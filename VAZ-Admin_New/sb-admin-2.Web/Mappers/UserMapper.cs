﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sb_admin_2.Web.Entity;
using sb_admin_2.Web.Models;
using System.Globalization;

namespace sb_admin_2.Web.Mappers
{
    public class UserMapper
    {
        ViewAdsZoneEntities _db = new ViewAdsZoneEntities();

        public List<User> AppUsers(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string UID = "")
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            DateTime CurrentDate = CurrentIndianDate();

            if (strFromDate != "")
            {
                FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (strToDate != "")
            {
                ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            var data = (from a in _db.tbl_Users
                        where (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                                && (LocationId == 0 || a.LocationId == LocationId)
                                && (UID == "" || a.UserId == UID)
                                && (Status == "" || (Status == "Open" ? a.IsActive == 1 : a.IsActive != 1))
                        select a).ToList();

            return (from a in data
                    orderby a.Id descending
                    select new User
                    {
                        Id = a.Id,
                        FirstName = a.FirstName,
                        LastName = a.LastName,
                        Mobile = a.Mobile,
                        Email = a.Email,
                        SubscriptionType = (a.SubscriptionEndDate != null ? (a.SubscriptionEndDate >= CurrentDate ? "Paid" : "Free") : "Free"),
                        SubscriptionPlan = a.SubscriptionPlan == null ? "" : a.SubscriptionPlan,
                        LocationName = a.LocationName,
                        Gender = a.Gender,
                        Dob = a.Dob,
                        IsNotificationEnabled = (a.IsNotificationEnabled == null || a.IsNotificationEnabled == "" || a.IsNotificationEnabled == "No") ? "No" : "Yes",
                        UserId = a.UserId,
                        Address = a.Address,
                        DeviceType = a.DeviceType,
                        DeviceToken = a.DeviceToken,
                        Status = a.IsActive == 1 ? "Open" : "Closed",
                        strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy")
                    }).ToList();
        }

        public List<UserRpt> AppUsersRpt(string strFromDate = "", string strToDate = "", long LocationId = 0, string Status = "", string UID = "")
        {
            DateTime FromDate = DateTime.Now;
            DateTime ToDate = DateTime.Now;
            DateTime CurrentDate = CurrentIndianDate();

            if (strFromDate != "")
            {
                FromDate = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (strToDate != "")
            {
                ToDate = DateTime.ParseExact(strToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            var data = (from a in _db.tbl_Users
                        where (strFromDate == "" || (a.CreatedAt >= FromDate && a.CreatedAt.Value <= ToDate))
                                && (LocationId == 0 || a.LocationId == LocationId)
                                && (UID == "" || a.UserId == UID)
                                && (Status == "" || (Status == "Open" ? a.IsActive == 1 : a.IsActive != 1))
                        select a).ToList();

            return (from a in data
                    orderby a.Id descending
                    select new UserRpt
                    {
                        FirstName = a.FirstName,
                        LastName = a.LastName,
                        Mobile = a.Mobile,
                        Email = a.Email,
                        Subscription = (a.SubscriptionEndDate != null ? (a.SubscriptionEndDate >= CurrentDate ? "Paid" : "Free") : "Free"),
                        Location = a.LocationName,
                        Status = a.IsActive == 1 ? "Open" : "Closed",
                        RegisteredOn = a.CreatedAt.Value.ToString("dd MMM yyyy"),
                        UID = a.UserId
                    }).ToList();
        }

        public User User(long UserId)
        {

            DateTime CurrentDate = CurrentIndianDate();

            var data = (from a in _db.tbl_Users
                        where a.Id == UserId
                        select a).ToList();

            return (from a in data
                    orderby a.Id descending
                    select new User
                    {
                        Id = a.Id,
                        FirstName = a.FirstName,
                        LastName = a.LastName,
                        Mobile = a.Mobile,
                        Email = a.Email,
                        SubscriptionType = (a.SubscriptionEndDate != null ? (a.SubscriptionEndDate >= CurrentDate ? "Paid" : "Free") : "Free"),
                        SubscriptionPlan = a.SubscriptionPlan == null ? "" : a.SubscriptionPlan,
                        LocationId = a.LocationId,
                        LocationName = a.LocationName,
                        Gender = a.Gender,
                        Dob = a.Dob,
                        IsNotificationEnabled = (a.IsNotificationEnabled == null || a.IsNotificationEnabled == "" || a.IsNotificationEnabled == "No") ? "No" : "Yes",
                        UserId = a.UserId,
                        Address = a.Address,
                        IsActive = a.IsActive,
                        strCreatedAt = a.CreatedAt.ToString(),
                        Status = a.IsActive == 1 ? "Open" : "Closed",
                        DeviceToken = a.DeviceToken,
                        DeviceType = a.DeviceType == null ? "" : a.DeviceType,
                        strUserId = a.UserId
                    }).SingleOrDefault();
        }

        public ActivityVM UserActivity(long UserId)
        {
            ActivityVM VM = new ActivityVM();

            VM.AddPoints = (from a in _db.tbl_UserActivity where a.UserId == UserId && a.Type == "ADD" && a.IsViewed == 1 select a.RewardPoints).Sum();
            VM.VideoPoints = (from a in _db.tbl_UserActivity where a.UserId == UserId && a.Type == "VIDEO" && a.IsViewed == 1 select a.RewardPoints).Sum();

            VM.TotalPoints = (VM.AddPoints == null ? 0 : VM.AddPoints) + (VM.VideoPoints == null ? 0 : VM.VideoPoints);

           var activity = (from a in _db.tbl_UserActivity
                                  where a.UserId == UserId && a.IsViewed == 1
                                  
                                  select a).ToList();

           VM.TransactionList = (from a in activity
                                 
                                  orderby a.Id descending
                                  select new Activity
                                  {
                                      Type = a.Type == "ADD" ? "Advertisement" : "Video",
                                      RewardPoints = a.RewardPoints.Value,
                                      ViewedDate = a.LastUpdatedAt.Value.ToString("dd MMM yyy HH:mm"),
                                      Title = (a.Type == "ADD" ? (from b in _db.tbl_Adds where b.Id == a.AddId select b.Name).FirstOrDefault() : (from b in _db.tbl_Videos where b.Id == a.AddId select b.Name).FirstOrDefault())
                                  }).ToList();


            var data = (from a in _db.tbl_RedemptionRequests where a.UserId == UserId select a).ToList();

            if (data != null)
            {
                VM.Redemptions = (from a in data
                                  select new Redemption
                                  {
                                      Id = a.Id,
                                      AccountHolderName = a.AccountHolderName,
                                      AccountNumber = a.AccountNumber,
                                      Amount = a.Amount,
                                      BankName = a.BankName,
                                      IFSC = a.IFSC,
                                      Mobile = a.Mobile,
                                      Status = a.IsPending == true ? "Pending" : "Completed",
                                      Points = a.Points,
                                      UserId = a.UserId,
                                      strCreatedAt = a.CreatedAt.Value.ToString("dd MMM yyyy HH:mm")
                                  }).ToList();
            }

            return VM;

        }

        public int UpdateProfile(User Obj)
        {
            LocationMapper _locationMapper = new LocationMapper();
            tbl_Users user = (from a in _db.tbl_Users where a.Id == Obj.Id select a).SingleOrDefault();

            if (Obj.FirstName != "" && Obj.FirstName != null) user.FirstName = Obj.FirstName;
            if (Obj.LastName != "" && Obj.LastName != null) user.LastName = Obj.LastName;
            if (Obj.Dob != "" && Obj.Dob != null) user.Dob = Obj.Dob;
            if (Obj.Gender != "" && Obj.Gender != null) user.Gender = Obj.Gender;
            if (Obj.Address != "" && Obj.Address != null) user.Address = Obj.Address;
            if (Obj.IsNotificationEnabled != "" && Obj.IsNotificationEnabled != null) user.IsNotificationEnabled = Obj.IsNotificationEnabled;
            if (Obj.Email != "" && Obj.Email != null) user.Email = Obj.Email;
            if (Obj.Mobile != "" && Obj.Mobile != null) user.Mobile = Obj.Mobile;
            if (Obj.IsActive != null) user.IsActive = Obj.IsActive;
            if (Obj.LocationId != null && Obj.LocationId != 0) user.LocationId = Obj.LocationId;
            if (Obj.LocationId != null && Obj.LocationId != 0)
            {
                user.LocationName = _locationMapper.SingleLocation(Obj.LocationId.Value).Name;
            }
            if (Obj.SubscriptionType != "" && Obj.SubscriptionType != null) user.SubscriptionType = Obj.SubscriptionType;

            return _db.SaveChanges();


        }

        public bool CheckMobile(string Mobile)
        {
            bool isExist = false;

            int recCount = (from a in _db.tbl_Users where a.Mobile == Mobile select a).Count();

            if (recCount > 0)
            {
                isExist = true;
            }

            return isExist;
        }

        private static DateTime CurrentIndianDate()
        {
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return currentDate;
        }

    }
}