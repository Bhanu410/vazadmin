﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
  public class LocationController : Controller
  {
    LocationMapper _mapper = new LocationMapper();

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetLocations()
    {
      List<Location> LocationList = _mapper.GetLocations();
      return View(LocationList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostLocations(long LocationId = 0)
    {
      return View(InitializeLocation(LocationId));
    }

    private Location InitializeLocation(long LocationId)
    {
      Location Obj;

      if (LocationId > 0)
      {
        Obj = _mapper.SingleLocation(LocationId);
        Obj.PageTitle = "Edit Location";
        Obj.ButtonTitle = "Update";
      }
      else
      {
        Obj = new Location();
        Obj.PageTitle = "Add Location";
        Obj.ButtonTitle = "Save";
      }
      return Obj;
    }

    [HttpPost]
    public ActionResult PostLocations(Location location)
    {

      if (location.Id > 0)
      {
        _mapper.UpdateLocation(location);
      }
      else
      {
        if (_mapper.GetLocationsByName(location.Name).Count() > 0)
        {
          ModelState.AddModelError("", "Location already exists");
          return View(InitializeLocation(location.Id));
        }

        _mapper.PostLocation(location);
      }

      return RedirectToAction("GetLocations");
    }

    public ActionResult DeleteLocation(long LocationId)
    {
      _mapper.DeleteLocation(LocationId);
      return RedirectToAction("GetLocations");
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetSubLocations()
    {
      List<SubLocation> Obj = _mapper.GetSubLocations();
      return View(Obj);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostSubLocations(long SubLocationId = 0)
    {
      return View(InitializeSubLocation(SubLocationId));
    }

    private SubLocation InitializeSubLocation(long SubLocationId)
    {
      SubLocation Obj;

      if (SubLocationId > 0)
      {
        Obj = _mapper.SingleSubLocation(SubLocationId);
        Obj.PageTitle = "Edit Sub Location";
        Obj.ButtonTitle = "Update";
      }
      else
      {
        Obj = new SubLocation();
        Obj.PageTitle = "Add Sub Location";
        Obj.ButtonTitle = "Save";
      }

      Obj.LocationList = fillLocationList();
      return Obj;
    }

    [HttpPost]
    public ActionResult PostSubLocations(SubLocation subLocation)
    {
      if (subLocation.Id > 0)
      {
        _mapper.UpdateSubLocation(subLocation);
      }
      else
      {
        if (_mapper.GetSubLocationsByName(subLocation.Name, subLocation.LocationId).Count() > 0)
        {
          ModelState.AddModelError("", "Sublocation already exists");
          return View(InitializeSubLocation(subLocation.Id));
        }

        _mapper.PostSubLocation(subLocation);
      }

      return RedirectToAction("GetSubLocations");
    }

    public ActionResult DeleteSubLocation(long SubLocationId)
    {
      _mapper.DeleteSubLocation(SubLocationId);
      return RedirectToAction("GetSubLocations");
    }

    private List<SelectListItem> fillLocationList()
    {
      List<SelectListItem> LocationList = new List<SelectListItem>();

      List<Location> locations = _mapper.GetLocations();

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      return LocationList;
    }
  }
}