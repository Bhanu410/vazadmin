﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
    public class UserController : Controller
    {
        UserMapper _mapper = new UserMapper();
        LocationMapper _locationMapper = new LocationMapper();

        [SessionTimeoutAttribute]
        public ActionResult Index()
        {
            List<User> Users = _mapper.AppUsers();
            return View(Users);
        }

        [SessionTimeoutAttribute]
        public ActionResult Detail(long UserId)
        {
            User user = _mapper.User(UserId);
            return View(user);
        }

        [SessionTimeoutAttribute]
        public ActionResult Activity(long UserId)
        {
            ActivityVM VM = _mapper.UserActivity(UserId);

            return View(VM);
        }

        [SessionTimeoutAttribute]
        public ActionResult EditProfile(long UserId)
        {
            UserVM VM = new UserVM();

            VM.LocationList = fillLocationList();
            VM.User = _mapper.User(UserId);

            return View(VM);
        }

        [HttpPost]
        public ActionResult EditProfile(UserVM VM)
        {
            _mapper.UpdateProfile(VM.User);
            return RedirectToAction("Index");
        }

        private List<SelectListItem> fillLocationList()
        {
            List<SelectListItem> LocationList = new List<SelectListItem>();

            List<Location> locations = _locationMapper.GetLocations();

            locations.ForEach(x =>
            {
                LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            return LocationList;
        }

        public JsonResult CheckExistingMobile(string Mobile)
        {
            bool ifMobileExist = false;

            try
            {

                ifMobileExist = _mapper.CheckMobile(Mobile);

                return Json(!ifMobileExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

    }
}