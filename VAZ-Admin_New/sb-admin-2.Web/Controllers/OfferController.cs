﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sb_admin_2.Web.Models;
using sb_admin_2.Web.Mappers;
using System.IO;

namespace sb_admin_2.Web.Controllers
{
  public class OfferController : Controller
  {
    OfferMapper _mapper = new OfferMapper();
    OfferVM VM = new OfferVM();
    LocationMapper _locationMapper = new LocationMapper();
    StoreMapper _storeMapper = new StoreMapper();

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetCategories()
    {
      List<OfferCategories> CategoryList = _mapper.GetCategories();
      return View(CategoryList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostCategories(long CategoryId = 0)
    {
      return View(InitializeCategory(CategoryId));
    }

    private OfferCategories InitializeCategory(long CategoryId)
    {
      OfferCategories Category;

      if (CategoryId != 0)
      {
        Category = _mapper.GetSingleCategory(CategoryId);
        Category.PageTitle = "Edit Category";
        Category.ButtonTitle = "Update";
      }
      else
      {
        Category = new OfferCategories();
        Category.PageTitle = "Add Category";
        Category.ButtonTitle = "Save";
      }

      return Category;
    }

    [HttpPost]
    public ActionResult PostCategories(OfferCategories CatObj)
    {
      string ChildFolder = "OfferCategories";



      if (CatObj.Id != 0)
      {
        if (CatObj.file == null)
        {
          CatObj.ImagePath = _mapper.GetSingleCategory(CatObj.Id).ImagePath;
        }
        else
        {
          CatObj.ImagePath = FileProcess(CatObj.file, ChildFolder);
        }

        _mapper.UpdateCategories(CatObj);
      }
      else
      {
        if (CatObj.file == null)
        {
          ModelState.AddModelError("", "Please upload category icon");

          return View(InitializeCategory(CatObj.Id));
        }

        if (_mapper.GetCategoriesByName(CatObj.Name).Count() > 0)
        {
          ModelState.AddModelError("", "Category already exists");
          return View(InitializeCategory(CatObj.Id));
        }

        CatObj.ImagePath = FileProcess(CatObj.file, ChildFolder);

        _mapper.PostCategories(CatObj);
      }



      return RedirectToAction("GetCategories");




    }


    public ActionResult DeleteCategory(long CategoryId)
    {
      _mapper.DeleteCategories(CategoryId);
      return RedirectToAction("GetCategories");
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetSubCategories()
    {
      List<OfferSubCategories> CategoryList = _mapper.GetSubCategories();
      return View(CategoryList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostSubCategories(long SubCatId = 0)
    {
      return View(InitializeSubCategory(SubCatId));
    }

    private OfferVM InitializeSubCategory(long SubCatId)
    {
      List<SelectListItem> SelectList = new List<SelectListItem>();

      List<OfferCategories> categories = _mapper.GetCategories().ToList();

      categories.ForEach(x =>
      {
        SelectList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.CategoryList = SelectList;

      if (SubCatId != 0)
      {
        VM.SubCategory = _mapper.GetSingleSubCategory(SubCatId);
      }
      else
      {
        VM.SubCategory = new OfferSubCategories();
      }

      return VM;
    }

    [HttpPost]
    public ActionResult PostSubCategories(OfferVM VM)
    {
      string ChildFolder = "OfferSubCategories";


      if (VM.SubCategory.Id != 0)
      {
        if (VM.SubCategory.file == null)
        {
          VM.SubCategory.ImagePath = _mapper.GetSingleSubCategory(VM.SubCategory.Id).ImagePath;
        }
        else
        {
          VM.SubCategory.ImagePath = FileProcess(VM.SubCategory.file, ChildFolder);
        }

        _mapper.UpdateSubCategories(VM.SubCategory);
      }
      else
      {
        if (VM.SubCategory.file == null)
        {
          ModelState.AddModelError("", "Please upload category icon");
          return View(InitializeSubCategory(VM.SubCategory.Id));
        }

        if (_mapper.GetSubCategoriesByName(VM.SubCategory.Name, VM.SubCategory.MasterId.Value).Count() > 0)
        {
          ModelState.AddModelError("", "Subcategory already exists");
          return View(InitializeSubCategory(VM.SubCategory.Id));
        }


        VM.SubCategory.ImagePath = FileProcess(VM.SubCategory.file, ChildFolder);


        _mapper.PostSubCategories(VM.SubCategory);
      }


      return RedirectToAction("GetSubCategories");


    }

    public ActionResult DeleteSubCategory(long SubCatId)
    {
      _mapper.DeleteSubCategories(SubCatId);
      return RedirectToAction("GetSubCategories");
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetOffers()
    {
      List<Offers> OffersList = _mapper.GetOffers();
      return View(OffersList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostOffers(long OfferId = 0)
    {
      return View(InitializeOffers(OfferId));
    }

    private OfferVM InitializeOffers(long OfferId)
    {
      List<SelectListItem> CategoryList = new List<SelectListItem>();
      List<SelectListItem> SubCategoryList = new List<SelectListItem>();
      List<SelectListItem> LocationList = new List<SelectListItem>();
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<SelectListItem> StoreList = new List<SelectListItem>();

      //Category Dropdown

      List<OfferCategories> categories = _mapper.GetCategories().ToList();

      categories.ForEach(x =>
      {
        CategoryList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.CategoryList = CategoryList;

      // SubCategory dropdown

      List<OfferSubCategories> subCategories;
      List<Location> locations;
      List<SubLocation> subLocations;
      List<Store> stores;

      if (OfferId != 0)
      {
        Offers Offer = _mapper.GetSingleOffer(OfferId);
        subCategories = _mapper.GetSubCategoriesByCatId(Offer.CategoryId == null ? 0 : Offer.CategoryId.Value).ToList();
        subLocations = _locationMapper.GetSubLocationsByLocation(Offer.LocationId == null ? 0 : Offer.LocationId.Value);
        stores = _storeMapper.StoreList(Offer.LocationId == null ? 0 : Offer.LocationId.Value, Offer.SubLocationId == null ? 0 : Offer.SubLocationId.Value);
      }
      else
      {
        subCategories = _mapper.GetSubCategoriesByCatId(0).ToList();
        subLocations = new List<SubLocation>();
        stores = new List<Store>();
      }


      subCategories.ForEach(x =>
      {
        SubCategoryList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.SubCategoryList = SubCategoryList;

      // Location dropdown

      locations = _locationMapper.GetLocations();

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.LocationList = LocationList;



      subLocations.ForEach(x =>
      {
        SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.SubLocationList = SubLocationList;



      stores.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.StoreList = StoreList;

      if (OfferId != 0)
      {
        VM.Offer = _mapper.GetSingleOffer(OfferId);
        VM.Offer.OfferImages = _mapper.GetOfferImages(OfferId);
      }
      else
      {
        VM.Offer = new Offers();
        VM.Offer.OfferImages = new List<OfferImages>();
      }

      return VM;
    }

    [HttpPost]
    public ActionResult PostOffers(OfferVM VM, HttpPostedFileBase[] files)
    {
      string ChildFolder = "Offers";

      if (VM.Offer.Id != 0)
      {
          if (files[0] == null)
          {
              if (_mapper.OfferImageCount(VM.Offer.Id) == 0)
              {
                  ModelState.AddModelError("", "Add atleast one offer image");
                  return View(InitializeOffers(VM.Offer.Id));
              }
          }
       
          _mapper.UpdateOffers(VM.Offer);
      }
      else
      {
          if (files[0] == null)
          {
              ModelState.AddModelError("", "Add atleast one offer image");
              return View(InitializeOffers(VM.Offer.Id));
          }

        VM.Offer.Id = _mapper.PostOffers(VM.Offer);
      }

      if (files[0] != null)
      {
        foreach (HttpPostedFileBase file in files)
        {
          OfferImages iObj = new OfferImages();
          iObj.OfferId = VM.Offer.Id;
          iObj.ImagePath = FileProcess(file, ChildFolder);

          _mapper.UploadMultipleOfferImages(iObj);
        }
      }
   


      return RedirectToAction("GetOffers");
    }

    private string FileProcess(HttpPostedFileBase file, string ChildFolder)
    {
      string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

      string path = Server.MapPath("~/Images/" + ChildFolder + "/");
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      string ActualPath = Path.Combine(path, Ticks + file.FileName);
      file.SaveAs(ActualPath);

      string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

      return AbsolutePath;

    }

    public ActionResult DeleteOffer(long OfferId)
    {
      _mapper.DeleteOffers(OfferId);
      return RedirectToAction("GetOffers");
    }

    public ActionResult FillSubCategoryDDL(long CatId)
    {
      List<SelectListItem> SubCategoryList = new List<SelectListItem>();
      List<OfferSubCategories> subCategories = _mapper.GetSubCategoriesByCatId(CatId).ToList();

      subCategories.ForEach(x =>
      {
        SubCategoryList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.SubCategoryList = SubCategoryList;
      return PartialView("_subCategoryDDL", VM);
    }

    public ActionResult FillSubLocationDDL(long LocationId)
    {
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<SubLocation> subLocations = _locationMapper.GetSubLocationsByLocation(LocationId).ToList();

      subLocations.ForEach(x =>
      {
        SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.SubLocationList = SubLocationList;
      return PartialView("_subLocationDDL", VM);
    }

    public ActionResult FillStoreDDL(long LocationId, long SubLocationId)
    {
      List<SelectListItem> StoreList = new List<SelectListItem>();
      List<Store> stores = _storeMapper.StoreList().Where(x => x.LocationId == LocationId && x.SubLocationId == SubLocationId).ToList();

      stores.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.StoreList = StoreList;
      return PartialView("_storeDDL", VM);
    }

    public ActionResult DeleteOfferImage(long OfferId, long ImageId)
    {
      Offers offer = new Offers();
      List<OfferImages> OfferImages = new List<Models.OfferImages>();

      _mapper.DeleteOfferImage(ImageId);

      OfferImages = _mapper.GetOfferImages(OfferId);

      offer.OfferImages = OfferImages;

      VM.Offer = offer;
      return PartialView("_offerImages", VM);
    }
  }
}