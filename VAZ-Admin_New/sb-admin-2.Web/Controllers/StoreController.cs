﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
    public class StoreController : Controller
    {
        StoreMapper _mapper = new StoreMapper();
        LocationMapper _locationMapper = new LocationMapper();
        StoreVM VM = new StoreVM();

        BannerMapper _bannerMapper = new BannerMapper();
        AdvertisementMapper _addMapper = new AdvertisementMapper();
        VideoMapper _videoMapper = new VideoMapper();
        OfferMapper _offerMapper = new OfferMapper();

        [HttpGet]
        [SessionTimeout]
        public ActionResult GetStores()
        {
            List<Store> StoresList = _mapper.StoreList();
            return View(StoresList);
        }

        [HttpGet]
        [SessionTimeout]
        public ActionResult AddStore(long StoreId = 0)
        {
            List<SelectListItem> LocationList = new List<SelectListItem>();
            List<SelectListItem> SubLocationList = new List<SelectListItem>();

            List<Location> locations = _locationMapper.GetLocations();
            List<SubLocation> subLocations;

            locations.ForEach(x =>
            {
                LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            VM.LocationList = LocationList;




            if (StoreId != 0)
            {
                VM.Store = _mapper.SingleStore(StoreId);
                subLocations = _locationMapper.GetSubLocationsByLocation(VM.Store.LocationId == null ? 0 : VM.Store.LocationId.Value);
            }
            else
            {
                VM.Store = new Store();
                subLocations = new List<SubLocation>();
            }



            subLocations.ForEach(x =>
            {
                SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            VM.SubLocationList = SubLocationList;

            return View(VM);
        }

        [HttpPost]
        public ActionResult AddStore(StoreVM VM)
        {
            string ChildFolder = "Stores";

            if (VM.Store.Id != 0)
            {
                VM.Store.ImagePath = (VM.Store.file == null ? _mapper.SingleStore(VM.Store.Id).ImagePath : FileProcess(VM.Store.file, ChildFolder));

                _mapper.UpdateStore(VM.Store);
            }
            else
            {
                VM.Store.ImagePath = (VM.Store.file == null ? "" : FileProcess(VM.Store.file, ChildFolder));

                _mapper.AddStore(VM.Store);
            }

            return RedirectToAction("GetStores");
        }

        public ActionResult DeleteStore(long StoreId)
        {
            _mapper.DeleteStore(StoreId);
            return RedirectToAction("GetStores");
        }

        private string FileProcess(HttpPostedFileBase file, string ChildFolder)
        {
            string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

            string path = Server.MapPath("~/Images/" + ChildFolder + "/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string ActualPath = Path.Combine(path, Ticks + file.FileName);
            file.SaveAs(ActualPath);

            string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

            return AbsolutePath;

        }

        public ActionResult FillSubLocationDDL(long LocationId)
        {
            List<SelectListItem> SubLocationList = new List<SelectListItem>();
            List<SubLocation> subLocations = _locationMapper.GetSubLocationsByLocation(LocationId).ToList();

            subLocations.ForEach(x =>
            {
                SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            VM.SubLocationList = SubLocationList;
            return PartialView("_subLocationDDL", VM);
        }

        [HttpGet]
        [SessionTimeout]
        public ActionResult Activity(string CustomerId)
        {

            VM.BannerList = _bannerMapper.GetBanners(CustomerId);
            VM.AddList = _addMapper.GetAdds(CustomerId);
            VM.VideoList = _videoMapper.GetVideos(CustomerId);
            VM.OfferList = _offerMapper.GetOffers(CustomerId);

            ViewBag.CustomerId = CustomerId;

            return View(VM);
        }

    }
}