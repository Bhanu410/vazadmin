﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System.IO;

namespace sb_admin_2.Web.Controllers
{
  public class BannerController : Controller
  {
    BannerMapper _mapper = new BannerMapper();
    LocationMapper _locationMapper = new LocationMapper();
    StoreMapper _storeMapper = new StoreMapper();
    BannerVM VM = new BannerVM();
    CustomerMapper _customerMapper = new CustomerMapper();

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetBanners()
    {
      List<Banner> BannerList = _mapper.GetBanners();

      return View(BannerList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostBanners(long BannerId = 0)
    {
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<SelectListItem> StoreList = new List<SelectListItem>();

      List<SubLocation> subLocations;
      List<Store> stores;
      List<Customer> customers;

      if (BannerId == 0)
      {
        VM.Banner = new Banner();
        subLocations = new List<SubLocation>();
        stores = new List<Store>();
       

        VM.Banner.PageTitle = "Add Banner";
        VM.Banner.ButtonTitle = "Save";
      }
      else
      {
        VM.Banner = _mapper.SingleBanner(BannerId);
        subLocations = _locationMapper.GetSubLocationsByLocation(VM.Banner.LocationId == null ? 0 : VM.Banner.LocationId.Value);
        stores = _storeMapper.StoreList(VM.Banner.LocationId == null ? 0 : VM.Banner.LocationId.Value, VM.Banner.SubLocationId == null ? 0 : VM.Banner.SubLocationId.Value);
       

        VM.Banner.PageTitle = "Edit Banner";
        VM.Banner.ButtonTitle = "Update";
      }

      VM.LocationList = fillLocationList(BannerId);



      subLocations.ForEach(x =>
      {
        SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.SubLocationList = SubLocationList;

      customers = _customerMapper.CustomerList();

      customers.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.CustomerId + ", " + x.Name, Value = x.Id.ToString() });
      });

      VM.StoreList = StoreList;

      return View(VM);
    }

    private List<SelectListItem> fillLocationList(long BannerId)
    {
      List<SelectListItem> LocationList = new List<SelectListItem>();

      List<Location> locations = _mapper.GetBannerLocations(BannerId);

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
      });

      return LocationList;
    }

    [HttpPost]
    public ActionResult PostBanners(BannerVM VM, HttpPostedFileBase[] files)
    {
      string ChildFolder = "Banners";

      if (VM.Banner.Id > 0)
      {
        if (VM.Banner.file == null)
        {
          VM.Banner.ImagePath = _mapper.SingleBanner(VM.Banner.Id).ImagePath;
        }
        else
        {
          VM.Banner.ImagePath = FileProcess(VM.Banner.file, ChildFolder);
        }

        _mapper.UpdateBanner(VM.Banner);
      }
      else
      {
        if (files[0] == null)
        {
          ModelState.AddModelError("", "Please upload banner");
          VM.Banner = new Banner();
          VM.Banner.PageTitle = "Add Banner";
          VM.Banner.ButtonTitle = "Save";
          VM.LocationList = fillLocationList(0);
          return View(VM);
        }

        foreach (HttpPostedFileBase file in files)
        {
          VM.Banner.ImagePath = FileProcess(file , ChildFolder);
          _mapper.PostBanner(VM.Banner);
          
        }
        
        
      }


      return RedirectToAction("GetBanners");
    }

    public ActionResult DeleteBanner(long BannerId)
    {
      _mapper.DeleteBanner(BannerId);
      return RedirectToAction("GetBanners");
    }

    private string FileProcess(HttpPostedFileBase file, string ChildFolder)
    {
      string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

      string path = Server.MapPath("~/Images/" + ChildFolder + "/");
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      string ActualPath = Path.Combine(path, Ticks + file.FileName);
      file.SaveAs(ActualPath);

      string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

      return AbsolutePath;

    }

    public ActionResult FillSubLocationDDL(string LocationId, long BannerId = 0)
    {
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<long> LocationList = new List<long>();

      if(LocationId.Length>0)
      {
        LocationId = LocationId.Trim(',');
        string[] strLocationArray = LocationId.Split(',');

        foreach (var item in strLocationArray)
        {
          LocationList.Add(long.Parse(item));
        }



        List<SubLocation> subLocations = _mapper.GetBannerSubLocations(LocationList, BannerId);

        subLocations.ForEach(x =>
        {
          SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
        });
      }
     

      VM.SubLocationList = SubLocationList;
      return PartialView("_subLocationDDL", VM);
    }

    public ActionResult FillStoreDDL(long LocationId, long SubLocationId)
    {


      List<SelectListItem> StoreList = new List<SelectListItem>();
      List<Store> stores = _storeMapper.StoreList().Where(x => x.LocationId == LocationId && x.SubLocationId == SubLocationId).ToList();
      List<Customer> customers = _customerMapper.CustomerList();

      customers.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.StoreList = StoreList;
      return PartialView("_storeDDL", VM);
    }
  }
}