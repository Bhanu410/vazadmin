﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sb_admin_2.Web.Models;
using sb_admin_2.Web.Mappers;
using System.IO;
using SelectPdf;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace sb_admin_2.Web.Controllers
{
    public class ReportsController : Controller
    {

        StoreMapper _storeMapper = new StoreMapper();
        BannerMapper _bannerMapper = new BannerMapper();
        AdvertisementMapper _addMapper = new AdvertisementMapper();
        VideoMapper _videoMapper = new VideoMapper();
        CustomerMapper _customerMapper = new CustomerMapper();
        UserMapper _userMapper = new UserMapper();
        LocationMapper _locationMapper = new LocationMapper();
        OfferMapper _offerMapper = new OfferMapper();


        [SessionTimeoutAttribute]
        public ActionResult Index(string ReportType)
        {
            Report Report = new Report();
            ReportFilter Filter = new ReportFilter();

            Filter.Type = ReportType;

            Report.LocationList = fillLocationList();

            Report.ReportFilter = Filter;

            if (ReportType == ReportTypes.ADD)
            {
                Report.Adds = _addMapper.GetAdds();
              
            }
            else if (ReportType == ReportTypes.VIDEO)
            {
                Report.Videos = _videoMapper.GetVideos();
               
            }
            else if (ReportType == ReportTypes.BANNER)
            {
                Report.Banners = _bannerMapper.GetBanners();
               
            }
            else if (ReportType == ReportTypes.USER)
            {
                Report.Users = _userMapper.AppUsers();
                
            }
            else if (ReportType == ReportTypes.CUSTOMER)
            {
                Report.Customers = _customerMapper.CustomerList();
               
            }
            else if (ReportType == ReportTypes.STORE)
            {
                Report.Stores = _storeMapper.StoreList();
               
            }
            else if (ReportType == ReportTypes.CUSTOMERACTIVITY)
            {
                Report.Adds = new List<Advertisement>();
                Report.Videos = new List<Video>();
                Report.Banners = new List<Banner>();
                Report.Offers = new List<Offers>();
            }

            Session["RVM"] = Report;

            return View(Report);
        }

    
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GetReportByType(Report RVM)
        {
            Report VM = new Report();

            Session["RVM"] = RVM;

            RVM.ReportFilter.Status = RVM.ReportFilter.Status == null ? "" : RVM.ReportFilter.Status;
            RVM.ReportFilter.CustomerId = RVM.ReportFilter.CustomerId == null ? "" : RVM.ReportFilter.CustomerId;
            RVM.ReportFilter.strToDate = RVM.ReportFilter.strToDate == null ? "" : RVM.ReportFilter.strToDate;
            RVM.ReportFilter.strFromDate = RVM.ReportFilter.strFromDate == null ? "" : RVM.ReportFilter.strFromDate;

            if (RVM.ReportFilter.Type == ReportTypes.ADD)
            {
                VM.Adds = _addMapper.GetAdds(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                return View("_Adds", VM.Adds);
            }
            else if (RVM.ReportFilter.Type == ReportTypes.VIDEO)
            {
                VM.Videos = _videoMapper.GetVideos(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                return View("_Videos", VM.Videos);
            }
            else if (RVM.ReportFilter.Type == ReportTypes.BANNER)
            {
                VM.Banners = _bannerMapper.GetBanners(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                return View("_Banners", VM.Banners);
            }
            else if (RVM.ReportFilter.Type == ReportTypes.USER)
            {
                VM.Users = _userMapper.AppUsers(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                return View("_Users", VM.Users);
            }
            else if (RVM.ReportFilter.Type == ReportTypes.CUSTOMER)
            {
                VM.Customers = _customerMapper.CustomerList(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                return View("_Customers", VM.Customers);
            }
            else if (RVM.ReportFilter.Type == ReportTypes.STORE)
            {
                VM.Stores = _storeMapper.StoreList(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                return View("_Stores", VM.Stores);
            }
            else if (RVM.ReportFilter.Type == ReportTypes.CUSTOMERACTIVITY)
            {
                VM.CustomerId = RVM.ReportFilter.CustomerId;
                VM.CustomerName = _customerMapper.SingleCustomer(RVM.ReportFilter.CustomerId).Name;
                VM.Banners = _bannerMapper.GetBanners("","",0,"",RVM.ReportFilter.CustomerId);
                VM.Adds = _addMapper.GetAdds("", "", 0, "", RVM.ReportFilter.CustomerId);
                VM.Videos = _videoMapper.GetVideos("", "", 0, "", RVM.ReportFilter.CustomerId);
               // VM.Offers = _offerMapper.GetOffers(RVM.ReportFilter.CustomerId);

                return View("_CustomerActivity", VM);
            }
            else
            {
                return null;
            }
        }

        
        [ValidateInput(false)]
        public ActionResult GetReportByType(string ReportType, string strFromDate = "", string strToDate = "", long Location = 0, string Status = "", string CustomerId = "")
        {
            Report VM = new Report();

            Status = Status == null ? "" : Status;
            CustomerId = CustomerId == null ? "" : CustomerId;
            strFromDate = strFromDate == null ? "" : strFromDate;
            strToDate = strToDate == null ? "" : strToDate;

            if (ReportType == ReportTypes.ADD)
            {
                VM.Adds = _addMapper.GetAdds(strFromDate, strToDate, Location, Status, CustomerId);
                return View("_Adds", VM.Adds);
            }
            else if (ReportType == ReportTypes.VIDEO)
            {
                VM.Videos = _videoMapper.GetVideos(strFromDate, strToDate, Location, Status, CustomerId);
                return View("_Videos", VM.Videos);
            }
            else if (ReportType == ReportTypes.BANNER)
            {
                VM.Banners = _bannerMapper.GetBanners(strFromDate, strToDate, Location, Status, CustomerId);
                return View("_Banners", VM.Banners);
            }
            else if (ReportType == ReportTypes.USER)
            {
                VM.Users = _userMapper.AppUsers(strFromDate, strToDate, Location, Status, CustomerId);
                return View("_Users", VM.Users);
            }
            else if (ReportType == ReportTypes.CUSTOMER)
            {
                VM.Customers = _customerMapper.CustomerList(strFromDate, strToDate, Location, Status, CustomerId);
                return View("_Customers", VM.Customers);
            }
            else if (ReportType == ReportTypes.STORE)
            {
                VM.Stores = _storeMapper.StoreList(strFromDate, strToDate, Location, Status, CustomerId);
                return View("_Stores", VM.Stores);
            }
            else if (ReportType == ReportTypes.CUSTOMERACTIVITY)
            {
                VM.CustomerId = CustomerId;
                VM.CustomerName = _customerMapper.SingleCustomer(CustomerId).Name;
                VM.Banners = _bannerMapper.GetBanners("", "", 0, "", CustomerId);
                VM.Adds = _addMapper.GetAdds("", "", 0, "", CustomerId);
                VM.Videos = _videoMapper.GetVideos("", "", 0, "", CustomerId);
               // VM.Offers = _offerMapper.GetOffers(CustomerId);

                return View("_CustomerActivity", VM);
            }
            else
            {
                return null;
            }
        }

        private List<SelectListItem> fillLocationList()
        {
            List<SelectListItem> LocationList = new List<SelectListItem>();

            List<Location> locations = _locationMapper.GetLocations();

            locations.ForEach(x =>
            {
                LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            return LocationList;
        }
      
        public ActionResult Export(string ReportType, string strFromDate = "" , string strToDate = "" , string strLocation = "" , string Status = "" , string CustomerId = "")
        {

            long lngLocation = 0;

            if (strLocation + "" != "")
            {
                lngLocation = long.Parse(strLocation);
            }

            var request = System.Web.HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;


            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            // read parameters from the webpage
            string url = baseUrl + "Reports/GetReportByType?ReportType=" + ReportType + "&strFromDate=" + strFromDate + "&strToDate=" + strToDate + "&Location=" + lngLocation + "&Status=" + Status + "&CustomerId=" + CustomerId;

            string pdf_page_size = "A4";
            PdfPageSize pageSize =
                (PdfPageSize)Enum.Parse(typeof(PdfPageSize), pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
        

            int webPageHeight = 0;
          

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertUrl(url);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;
        }

        public ActionResult ExportToExcel()
        {
            var gv = new GridView();

            Report RVM = (Report)Session["RVM"];

            RVM.ReportFilter.Status = RVM.ReportFilter.Status == null ? "" : RVM.ReportFilter.Status;
            RVM.ReportFilter.CustomerId = RVM.ReportFilter.CustomerId == null ? "" : RVM.ReportFilter.CustomerId;
            RVM.ReportFilter.strToDate = RVM.ReportFilter.strToDate == null ? "" : RVM.ReportFilter.strToDate;
            RVM.ReportFilter.strFromDate = RVM.ReportFilter.strFromDate == null ? "" : RVM.ReportFilter.strFromDate;

            if (RVM.ReportFilter.Type == ReportTypes.ADD)
            {
                gv.DataSource = _addMapper.GetAddsReport(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
               
            }
            else if (RVM.ReportFilter.Type == ReportTypes.VIDEO)
            {
                gv.DataSource = _videoMapper.GetVideosReport(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
               
            }
            else if (RVM.ReportFilter.Type == ReportTypes.BANNER)
            {
                gv.DataSource = _bannerMapper.GetBannersRpt(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
              
            }
            else if (RVM.ReportFilter.Type == ReportTypes.USER)
            {
                gv.DataSource = _userMapper.AppUsersRpt(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
               
            }
            else if (RVM.ReportFilter.Type == ReportTypes.CUSTOMER)
            {
                gv.DataSource = _customerMapper.CustomerListRpt(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                
            }
            else if (RVM.ReportFilter.Type == ReportTypes.STORE)
            {
                gv.DataSource = _storeMapper.StoreListRpt(RVM.ReportFilter.strFromDate.ToString(), RVM.ReportFilter.strToDate.ToString(), RVM.ReportFilter.LocationId, RVM.ReportFilter.Status, RVM.ReportFilter.CustomerId);
                
            }
            else if (RVM.ReportFilter.Type == ReportTypes.CUSTOMERACTIVITY)
            {
                List<CustomerActivityReport> AddActivity = new List<CustomerActivityReport>();
                List<CustomerActivityReport> VideoActivity = new List<CustomerActivityReport>();
                List<CustomerActivityReport> BannerActivity = new List<CustomerActivityReport>();

                AddActivity = _addMapper.GetAddsReportCustom("", "", 0, "", RVM.ReportFilter.CustomerId);
                VideoActivity = _videoMapper.GetVideosCustom("", "", 0, "", RVM.ReportFilter.CustomerId);
                BannerActivity = _bannerMapper.GetBannersCustom("", "", 0, "", RVM.ReportFilter.CustomerId);

                var list = AddActivity.Concat(VideoActivity).Concat(BannerActivity).ToList();

                gv.DataSource = list;
                
            }
       
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Excel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("Index", RVM.ReportFilter.Type);
           
        } 
	}
}