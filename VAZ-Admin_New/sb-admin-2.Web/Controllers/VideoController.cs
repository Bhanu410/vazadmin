﻿using sb_admin_2.Web.Models;
using sb_admin_2.Web.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace sb_admin_2.Web.Controllers
{
  public class VideoController : Controller
  {
    VideoMapper _mapper = new VideoMapper();
    LocationMapper _locationMapper = new LocationMapper();
    StoreMapper _storeMapper = new StoreMapper();
    CustomerMapper _customerMapper = new CustomerMapper();

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetVideos()
    {
      List<Video> VideoList = _mapper.GetVideos();
      return View(VideoList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostVideo(long VideoId = 0)
    {
      Video video;
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<SelectListItem> StoreList = new List<SelectListItem>();

      List<SubLocation> subLocations;
      List<Store> stores;
      List<Customer> customers;

      if (VideoId > 0)
      {
        video = _mapper.SingleVideo(VideoId);
        subLocations = _locationMapper.GetSubLocationsByLocation(video.LocationId == null ? 0 : video.LocationId.Value);
        stores = _storeMapper.StoreList(video.LocationId == null ? 0 : video.LocationId.Value, video.SubLocationId == null ? 0 : video.SubLocationId.Value);
       

        video.PageTitle = "Edit Video";
        video.ButtonTitle = "Update";
      }
      else
      {
        video = new Video();
        video.ViewsLimit = 10000;
        subLocations = new List<SubLocation>();
        stores = new List<Store>();
       

        video.PageTitle = "Add Video";
        video.ButtonTitle = "Save";
      }

      video.LocationList = fillLocation(VideoId);



      subLocations.ForEach(x =>
      {
        SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      video.SubLocationList = SubLocationList;

      customers = _customerMapper.CustomerList();

      customers.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.CustomerId + ", " + x.Name, Value = x.Id.ToString() });
      });

      video.StoreList = StoreList;

      return View(video);
    }

    private List<SelectListItem> fillLocation(long VideoId)
    {
      List<SelectListItem> LocationList = new List<SelectListItem>();

      List<Location> locations = _mapper.GetVideoLocations(VideoId);

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
      });

      return LocationList;
    }

    [HttpPost]
    public ActionResult PostVideo(Video Obj)
    {
      string ChildFolder = "Videos";

      if (Obj.Id > 0)
      {
        //if (Obj.file == null)
        //{
        //    Obj.VideoPath = _mapper.SingleVideo(Obj.Id).VideoPath;
        //}
        //else
        //{
        //    Obj.VideoPath = FileProcess(Obj.file, ChildFolder);
        //}

        _mapper.UpdateVideo(Obj);
      }
      else
      {
        //if (Obj.file == null)
        //{
        //    ModelState.AddModelError("", "Please upload Video");
        //    Obj = new Video();
        //    Obj.LocationList = fillLocation();
        //    return View(Obj);
        //}

        //Obj.VideoPath = FileProcess(Obj.file, ChildFolder);

        _mapper.PostVideo(Obj);
      }

      return RedirectToAction("GetVideos");
    }

    public ActionResult DeleteVideo(long VideoId)
    {
      _mapper.DeleteVideo(VideoId);
      return RedirectToAction("GetVideos");
    }

    private string FileProcess(HttpPostedFileBase file, string ChildFolder)
    {
      string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

      string path = Server.MapPath("~/Images/" + ChildFolder + "/");
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      string ActualPath = Path.Combine(path, Ticks + file.FileName);
      file.SaveAs(ActualPath);

      string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

      return AbsolutePath;

    }

    public ActionResult FillSubLocationDDL(string LocationId, long VideoId)
    {
      Video video = new Video();

      List<long> LocationList = new List<long>();

      List<SelectListItem> SubLocationList = new List<SelectListItem>();

      if (LocationId.Length > 0)
      {
        LocationId = LocationId.Trim(',');
        string[] strLocationArray = LocationId.Split(',');

        foreach (var item in strLocationArray)
        {
          LocationList.Add(long.Parse(item));
        }



        List<SubLocation> subLocations = _mapper.GetVideoSubLocations(LocationList, VideoId);

        subLocations.ForEach(x =>
        {
          SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
        });
      }

      video.SubLocationList = SubLocationList;
      return PartialView("_subLocationDDL", video);
    }

    public ActionResult FillStoreDDL(long LocationId, long SubLocationId)
    {
      Video video = new Video();

      List<SelectListItem> StoreList = new List<SelectListItem>();
      List<Store> stores = _storeMapper.StoreList().Where(x => x.LocationId == LocationId && x.SubLocationId == SubLocationId).ToList();
      List<Customer> customers = _customerMapper.CustomerList();

      customers.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      video.StoreList = StoreList;
      return PartialView("_storeDDL", video);
    }

    public ActionResult Reassign(long VideoId)
    {
      _mapper.Reassign(VideoId);
      return RedirectToAction("GetVideos");
    }

    public JsonResult MultipleReassign(string Ids)
    {
      string[] IdsArray = Ids.Split(',');

      foreach (string Id in IdsArray)
      {
        if (Id != "selectAll")
          _mapper.Reassign(long.Parse(Id));
      }

      return Json("Success", JsonRequestBehavior.AllowGet);
    }
  }
}