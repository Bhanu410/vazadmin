﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
  public class MagazineController : Controller
  {
    MagazineMapper _mapper = new MagazineMapper();
    MagazineVM VM = new MagazineVM();
    LocationMapper _locationMapper = new LocationMapper();

    [SessionTimeoutAttribute]
    public ActionResult GetMagazine()
    {
      List<Magazine> MagazineList = _mapper.GetMagazineList();
      return View(MagazineList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostMagazine(long MagazineId = 0)
    {
      return View(InitializeMagazine(MagazineId));
    }

    private MagazineVM InitializeMagazine(long MagazineId)
    {

      List<SelectListItem> LocationList = new List<SelectListItem>();
 
      List<Location> locations;



      // Location dropdown

      locations = _mapper.GetMagazineLocations(MagazineId);

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
      });

      VM.LocationList = LocationList;


      if (MagazineId != 0)
      {
        VM.Magazine = _mapper.GetSingleMagazineObj(MagazineId);
        VM.Magazine.MagazineImages = _mapper.GetMagazineImages(MagazineId);
        VM.Magazine.PageTitle = "Edit Magazine";
        VM.Magazine.ButtonTitle = "Update";
      }
      else
      {
        VM.Magazine = new Magazine();
        VM.Magazine.MagazineImages = new List<MagazineImages>();
        VM.Magazine.PageTitle = "Add Magazine";
        VM.Magazine.ButtonTitle = "Save";
      }

      return VM;
    }

    [HttpPost]
    [ValidateInput(false)]
    public ActionResult PostMagazine(MagazineVM VM, HttpPostedFileBase[] files)
    {
      string ChildFolder = "Magazine";

      if (VM.Magazine.Id != 0)
      {
        if (files[0] == null)
        {
          if (_mapper.MagazineImageCount(VM.Magazine.Id) == 0)
          {
            ModelState.AddModelError("", "Add atleast one image");
            return View(InitializeMagazine(VM.Magazine.Id));
          }
        }

        if (VM.Magazine.file == null)
        {
          VM.Magazine.ImagePath = _mapper.GetSingleMagazineObj(VM.Magazine.Id).ImagePath;
        }
        else
        {
          VM.Magazine.ImagePath = FileProcess(VM.Magazine.file, ChildFolder);
        }

        _mapper.UpdateMagazine(VM.Magazine);
      }
      else
      {
        if (files[0] == null)
        {
          ModelState.AddModelError("", "Add atleast one image");
          return View(InitializeMagazine(VM.Magazine.Id));
        }

        if (VM.Magazine.file == null)
        {
          ModelState.AddModelError("", "Please upload category icon");

          return View(InitializeMagazine(VM.Magazine.Id));
        }

        VM.Magazine.ImagePath = FileProcess(VM.Magazine.file, ChildFolder);

        VM.Magazine.Id = _mapper.PostMagazine(VM.Magazine);
      }

      if (files[0] != null)
      {
        foreach (HttpPostedFileBase file in files)
        {
          MagazineImages iObj = new MagazineImages();
          iObj.MagazineId = VM.Magazine.Id;
          iObj.ImagePath = FileProcess(file, ChildFolder);

          _mapper.UploadMultipleMagazineImages(iObj);
        }
      }



      return RedirectToAction("GetMagazine");
    }

    private string FileProcess(HttpPostedFileBase file, string ChildFolder)
    {
      string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

      string path = Server.MapPath("~/Images/" + ChildFolder + "/");
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      string ActualPath = Path.Combine(path, Ticks + file.FileName);
      file.SaveAs(ActualPath);

      string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

      return AbsolutePath;

    }

    public ActionResult DeleteMagazine(long MagazineId)
    {
      _mapper.DeleteMagazine(MagazineId);
      return RedirectToAction("GetMagazine");
    }
    
    public ActionResult DeleteMagazineImage(long MagazineId, long ImageId)
    {
      Magazine Magazine = new Magazine();
      List<MagazineImages> MagazineImages = new List<MagazineImages>();

      _mapper.DeleteMagazineImage(ImageId);

      MagazineImages = _mapper.GetMagazineImages(MagazineId);

      Magazine.MagazineImages = MagazineImages;

      VM.Magazine = Magazine;
      return PartialView("_magazineImages", VM);
    }
  }
}