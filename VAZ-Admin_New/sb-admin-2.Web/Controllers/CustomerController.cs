﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
    public class CustomerController : Controller
    {
        CustomerMapper _mapper = new CustomerMapper();
        LocationMapper _locationMapper = new LocationMapper();
        CustomerVM VM = new CustomerVM();

        [HttpGet]
        [SessionTimeout]
        public ActionResult GetCustomers()
        {
            List<Customer> CustomerList = _mapper.CustomerList();
            return View(CustomerList);
        }

        [HttpGet]
        [SessionTimeout]
        public ActionResult AddCustomer(long CustomerId = 0)
        {
            List<SelectListItem> LocationList = new List<SelectListItem>();
            List<SelectListItem> SubLocationList = new List<SelectListItem>();

            List<Location> locations = _locationMapper.GetLocations();
            List<SubLocation> subLocations;

            locations.ForEach(x =>
            {
                LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            VM.LocationList = LocationList;




            if (CustomerId != 0)
            {
                VM.Customer = _mapper.SingleCustomer(CustomerId);
                subLocations = _locationMapper.GetSubLocationsByLocation(VM.Customer.LocationId == null ? 0 : VM.Customer.LocationId.Value);
            }
            else
            {
                VM.Customer = new Customer();
                subLocations = new List<SubLocation>();
            }



            subLocations.ForEach(x =>
            {
                SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            VM.SubLocationList = SubLocationList;

            return View(VM);
        }

        [HttpPost]
        public ActionResult AddCustomer(CustomerVM VM)
        {
            if (VM.Customer.Id != 0)
            {
                _mapper.UpdateCustomer(VM.Customer);
            }
            else
            {
                _mapper.AddCustomer(VM.Customer);
            }

            return RedirectToAction("GetCustomers");
        }

        public ActionResult DeleteCustomer(long CustomerId)
        {
            _mapper.DeleteCustomer(CustomerId);
            return RedirectToAction("GetCustomers");
        }

      

        public ActionResult FillSubLocationDDL(long LocationId)
        {
            List<SelectListItem> SubLocationList = new List<SelectListItem>();
            List<SubLocation> subLocations = _locationMapper.GetSubLocationsByLocation(LocationId).ToList();

            subLocations.ForEach(x =>
            {
                SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            });

            VM.SubLocationList = SubLocationList;
            return PartialView("_subLocationDDL", VM);
        }

        public JsonResult CheckCustomer(CustomerVM VM)
        {
            bool ifCustomerExist = false;

            try
            {

                ifCustomerExist = _mapper.SingleCustomer(VM.Customer.CustomerId) == null ? false : true ;

                return Json(ifCustomerExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

	}
}