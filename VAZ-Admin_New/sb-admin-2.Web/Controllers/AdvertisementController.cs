﻿using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sb_admin_2.Web.Mappers;
using System.IO;

namespace sb_admin_2.Web.Controllers
{
  public class AdvertisementController : Controller
  {
    AdvertisementMapper _mapper = new AdvertisementMapper();
    LocationMapper _locationMapper = new LocationMapper();
    StoreMapper _storeMapper = new StoreMapper();
    CustomerMapper _customerMapper = new CustomerMapper();

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult GetAdds()
    {
      List<Advertisement> AddsList = _mapper.GetAdds();
      return View(AddsList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostAdd(long AdvertisementId = 0)
    {

      Advertisement Add;
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<SelectListItem> StoreList = new List<SelectListItem>();

      List<SubLocation> subLocations;
      List<Store> stores;
      List<Customer> customers;

      if (AdvertisementId > 0)
      {
        Add = _mapper.SingleAdd(AdvertisementId);
        subLocations = _locationMapper.GetSubLocationsByLocation(Add.LocationId == null ? 0 : Add.LocationId.Value);
        stores = _storeMapper.StoreList(Add.LocationId == null ? 0 : Add.LocationId.Value, Add.SubLocationId == null ? 0 : Add.SubLocationId.Value);
      

        Add.PageTitle = "Edit Advertisement";
        Add.ButtonTitle = "Update";
      }
      else
      {
        Add = new Advertisement();
        Add.ViewsLimit = 10000;
        subLocations = new List<SubLocation>();
        stores = new List<Store>();
        

        Add.PageTitle = "Add Advertisement";
        Add.ButtonTitle = "Save";
      }

      Add.LocationList = fillLocation(AdvertisementId);

      subLocations.ForEach(x =>
      {
        SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      Add.SubLocationList = SubLocationList;

      customers = _customerMapper.CustomerList();

      customers.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.CustomerId+", " + x.Name, Value = x.Id.ToString() });
      });

      Add.StoreList = StoreList;


      return View(Add);
    }

    private List<SelectListItem> fillLocation(long AdvertisementId)
    {
      List<SelectListItem> LocationList = new List<SelectListItem>();
     
      List<Location> locations = _mapper.GetAddsLocations(AdvertisementId);

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
      });


      return LocationList;
    }

    [HttpPost]
    public ActionResult PostAdd(Advertisement Obj)
    {
      string ChildFoler = "Adds";

      if (Obj.Id > 0)
      {
        if (Obj.file == null)
        {
          Obj.ImagePath = _mapper.SingleAdd(Obj.Id).ImagePath;
        }
        else
        {
          Obj.ImagePath = FileProcess(Obj.file, ChildFoler);
        }

        _mapper.UpdateAdd(Obj);
      }
      else
      {
        if (Obj.file == null)
        {
          ModelState.AddModelError("", "Please upload Advertisement");
          Obj = new Advertisement();
          Obj.LocationList = fillLocation(0);
          Obj.PageTitle = "Add Advertisement";
          Obj.ButtonTitle = "Save";
          return View(Obj);
        }

        Obj.ImagePath = FileProcess(Obj.file, ChildFoler);

        _mapper.PostAdd(Obj);
      }

      return RedirectToAction("GetAdds");
    }

    public ActionResult DeleteAdd(long AdvertisementId)
    {
      _mapper.DeleteAdd(AdvertisementId);
      return RedirectToAction("GetAdds");
    }

    private string FileProcess(HttpPostedFileBase file, string ChildFolder)
    {
      string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

      string path = Server.MapPath("~/Images/" + ChildFolder + "/");
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      string ActualPath = Path.Combine(path, Ticks + file.FileName);
      file.SaveAs(ActualPath);

      string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

      return AbsolutePath;

    }

    public ActionResult FillSubLocationDDL(string LocationId, long AdvertisementId)
    {
      Advertisement Add = new Advertisement();
      List<long> LocationList = new List<long>();

      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      if (LocationId.Length > 0)
      {
        LocationId = LocationId.Trim(',');
        string[] strLocationArray = LocationId.Split(',');

        foreach (var item in strLocationArray)
        {
          LocationList.Add(long.Parse(item));
        }



        List<SubLocation> subLocations = _mapper.GetAddsSubLocations(LocationList, AdvertisementId);

        subLocations.ForEach(x =>
        {
          SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
        });
      }

      Add.SubLocationList = SubLocationList;
      return PartialView("_subLocationDDL", Add);
    }

    public ActionResult FillStoreDDL(long LocationId, long SubLocationId)
    {
      Advertisement Add = new Advertisement();

      List<SelectListItem> StoreList = new List<SelectListItem>();
      List<Store> stores = _storeMapper.StoreList().Where(x => x.LocationId == LocationId && x.SubLocationId == SubLocationId).ToList();
      List<Customer> customers = _customerMapper.CustomerList();

      customers.ForEach(x =>
      {
        StoreList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      Add.StoreList = StoreList;
      return PartialView("_storeDDL", Add);
    }

    public ActionResult Reassign(long AdvertisementId)
    {
      _mapper.Reassign(AdvertisementId);
      return RedirectToAction("GetAdds");
    }

    public JsonResult MultipleReassign(string Ids)
    {
      string[] IdsArray = Ids.Split(',');

      foreach (string Id in IdsArray)
      {
        if (Id != "selectAll")
          _mapper.Reassign(long.Parse(Id));
      }

      return Json("Success", JsonRequestBehavior.AllowGet);
    }
  }
}