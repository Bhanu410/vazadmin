﻿using Newtonsoft.Json;
using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
    public class HomeController : Controller
    {

        DashBoard VM = new DashBoard();

        DashboardMapper _mapper = new DashboardMapper();

        JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };



        [SessionTimeoutAttribute]
        public ActionResult Index()
        {
            VM = _mapper.ConsolidatedNumbers();
            return View(VM);
        }

        public JsonResult CharInit()
        {
            VM.UserDataPoints = JsonConvert.SerializeObject(_mapper.ConsolidatedValues("USER"), _jsonSetting);
            VM.AddDataPoints = JsonConvert.SerializeObject(_mapper.ConsolidatedValues("ADD"), _jsonSetting);
            VM.VideoDataPoints = JsonConvert.SerializeObject(_mapper.ConsolidatedValues("VIDEO"), _jsonSetting);
            VM.CustomerDataPoints = JsonConvert.SerializeObject(_mapper.ConsolidatedValues("CUSTOMER"), _jsonSetting);

            return Json(VM, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FlotCharts()
        {
            return View("FlotCharts");
        }

        public ActionResult MorrisCharts()
        {
            return View("MorrisCharts");
        }

        public ActionResult Tables()
        {
            return View("Tables");
        }

        public ActionResult Forms()
        {
            return View("Forms");
        }

        public ActionResult Panels()
        {
            return View("Panels");
        }

        public ActionResult Buttons()
        {
            return View("Buttons");
        }

        public ActionResult Notifications()
        {
            return View("Notifications");
        }

        public ActionResult Typography()
        {
            return View("Typography");
        }

        public ActionResult Icons()
        {
            return View("Icons");
        }

        public ActionResult Grid()
        {
            return View("Grid");
        }

        public ActionResult Blank()
        {
            return View("Blank");
        }

        public ActionResult Login()
        {
            return View("Login");
        }

       

    }

    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["UserName"] == null)
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}