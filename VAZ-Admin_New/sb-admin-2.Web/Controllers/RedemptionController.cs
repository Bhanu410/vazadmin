﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
  public class RedemptionController : Controller
  {
    RedemptionMapper _mapper = new RedemptionMapper();
    GlobalValuesMapper _globalsMapper = new GlobalValuesMapper();

    [SessionTimeoutAttribute]
    public ActionResult Requests(string Type)
    {
      bool isPending = (Type == "Pending" ? true : false);

      List<Redemption> listObj = _mapper.getRequests(isPending);
      return View(listObj);
    }

    public ActionResult Approve(long RequestId)
    {
      Notification nObj = new Notification();
      Redemption Request = _mapper.RequestDetail(RequestId);
      User user = _mapper.getUserByRequestId(RequestId);
      string strApplicationId = _globalsMapper.Globals().Where(a => a.Name == "Android_PushNotification_APIKey").Select(a => a.Value).First();
      string strSenderId = _globalsMapper.Globals().Where(a => a.Name == "Android_PushNotification_SenderId").Select(a => a.Value).First();
     

      int i = _mapper.ApproveRequest(RequestId);

      if (i > 0)
      {
        nObj.SentTo = user.FirstName + " " + user.LastName;
        nObj.Mobile = user.Mobile;
        nObj.CreatedAt = CurrentIndianDate();
        nObj.Title = "Redemption Initiated";
        nObj.Message = "Amount of Rs " + Request.Amount + " will be credited to your bank account soon!";
        nObj.ID = RandomNumber(100000, 999999);

        PushNotification(nObj, user, strApplicationId, strSenderId);
      }


      return RedirectToAction("Requests", new { Type = "Pending" });
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PointsDefinition()
    {
      PointsDefinition pObj = _mapper.getDefinition();
      return View(pObj);
    }

    [HttpPost]
    public ActionResult PointsDefinition(PointsDefinition pObj)
    {
      _mapper.UpdateDefinition(pObj);
      PointsDefinition Obj = _mapper.getDefinition();
      return View(Obj);
    }


    public static void PushNotification(Notification nObj, User user, string strApplicationId, string strSenderId)
    {
      string applicationID = strApplicationId;
      string senderId = strSenderId;

      string deviceId = user.DeviceToken;
      WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
      tRequest.Method = "post";
      tRequest.ContentType = "application/json";

      Byte[] byteArray = null;


      byteArray = Encoding.UTF8.GetBytes("{\"to\" : \"" + deviceId + "\",\"data\" : {\"message\":{\"Title\":\"" + nObj.Title + "\",\"ID\":\"" + nObj.ID + "\",\"Message\":\"" + nObj.Message + "\",\"Date\":\"" + nObj.CreatedAt.Value.ToString("dd MMM yyyy HH:mm:ss tt") + "\"}}}");


      tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
      tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
      tRequest.ContentLength = byteArray.Length;

      using (Stream dataStream = tRequest.GetRequestStream())
      {
        dataStream.Write(byteArray, 0, byteArray.Length);
        using (WebResponse tResponse = tRequest.GetResponse())
        {
          using (Stream dataStreamResponse = tResponse.GetResponseStream())
          {
            using (StreamReader tReader = new StreamReader(dataStreamResponse))
            {
              String sResponseFromServer = tReader.ReadToEnd();
              string str = sResponseFromServer;
            }
          }
        }
      }
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }

    public int RandomNumber(int min, int max)
    {
      Random random = new Random();
      return random.Next(min, max);
    }

  }
}