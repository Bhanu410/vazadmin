﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Project.MoonAPNS;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;

namespace sb_admin_2.Web.Controllers
{
  public class NotificationController : Controller
  {
    UserMapper _userMapper = new UserMapper();
    GlobalValuesMapper _globalsMapper = new GlobalValuesMapper();
    NotificationMapper _mapper = new NotificationMapper();

    Notification nObj = new Notification();

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult SendNotification()
    {
      nObj.UsersList = fillUsersList();
      return View(nObj);
    }

    public ActionResult SendNotification(Notification nObj)
    {

      User user = _userMapper.User(nObj.UserId);
      string strApplicationId = _globalsMapper.Globals().Where(a => a.Name == "Android_PushNotification_APIKey").Select(a => a.Value).First();
      string strSenderId = _globalsMapper.Globals().Where(a => a.Name == "Android_PushNotification_SenderId").Select(a => a.Value).First();
      string strFCMUsers = _globalsMapper.Globals().Where(a => a.Name == "FCMALLUSERS").Select(a => a.Value).First();

      nObj.FCMUsers = strFCMUsers;

      try
      {

        if (nObj.For.ToUpper() == "SINGLEUSER")
        {
          if (user.DeviceType.ToUpper() == "ANDROID")
          {
            nObj.SentTo = user.UserId +", "+ user.FirstName + " " + user.LastName;
            nObj.Mobile = user.Mobile;
            nObj.CreatedAt = CurrentIndianDate();
            nObj.ID = _mapper.RecordNotificationData(nObj);

            PushNotification(nObj, user, strApplicationId, strSenderId, nObj.For.ToUpper());
          }
          else if (user.DeviceType.ToUpper() == "IOS")
          {
            PushNotificationToApple(nObj, user);
          }


        }
        else
        {
          List<User> UsersList = _userMapper.AppUsers().Where(a => a.DeviceType == "Android" && a.DeviceToken != null).ToList();

          nObj.SentTo = nObj.For;
          nObj.CreatedAt = CurrentIndianDate();
          nObj.ID = _mapper.RecordNotificationData(nObj);

          PushNotification(nObj, user, strApplicationId, strSenderId, "");

        }


      }
      catch (Exception ex)
      {
        string str = ex.Message;
      }

      TempData["msg"] = "<script>alert('Notification Sent.');</script>";

      nObj.UsersList = fillUsersList();

      ViewBag.Message = "Notification sent.";

      return View(nObj);



    }

    public static void PushNotification(Notification nObj, User user, string strApplicationId, string strSenderId, string userType)
    {
      string applicationID = strApplicationId;
      string senderId = strSenderId;

      string deviceId = user.DeviceToken;
      WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
      tRequest.Method = "post";
      tRequest.ContentType = "application/json";

      Byte[] byteArray = null;

      if (userType.ToUpper() == "SINGLEUSER")
      {
        byteArray = Encoding.UTF8.GetBytes("{\"to\" : \"" + deviceId + "\",\"data\" : {\"message\":{\"Title\":\"" + nObj.Title + "\",\"ID\":\"" + nObj.ID + "\",\"Message\":\"" + nObj.Message + "\",\"Date\":\"" + nObj.CreatedAt.Value.ToString("dd MMM yyyy HH:mm:ss tt") + "\"}}}");
      }
      else
      {
        string strPayLoad = "{\"to\" : \"/topics/" + nObj.FCMUsers + "\",\"data\" : {\"message\":{\"Title\":\"" + nObj.Title + "\",\"ID\":\"" + nObj.ID + "\",\"Message\":\"" + nObj.Message + "\",\"Date\":\"" + nObj.CreatedAt.Value.ToString("dd MMM yyyy HH:mm:ss tt") + "\"}}}";
        byteArray = Encoding.UTF8.GetBytes(strPayLoad);

      }

      tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
      tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
      tRequest.ContentLength = byteArray.Length;

      using (Stream dataStream = tRequest.GetRequestStream())
      {
        dataStream.Write(byteArray, 0, byteArray.Length);
        using (WebResponse tResponse = tRequest.GetResponse())
        {
          using (Stream dataStreamResponse = tResponse.GetResponseStream())
          {
            using (StreamReader tReader = new StreamReader(dataStreamResponse))
            {
              String sResponseFromServer = tReader.ReadToEnd();
              string str = sResponseFromServer;
            }
          }
        }
      }
    }


    private List<SelectListItem> fillUsersList()
    {
      List<SelectListItem> UsersList = new List<SelectListItem>();

      List<User> users = _userMapper.AppUsers();

      users.ForEach(x =>
      {
        UsersList.Add(new SelectListItem { Text = x.UserId+", "+ x.LastName + " " + x.FirstName, Value = x.Id.ToString() });
      });

      return UsersList;
    }

    public ActionResult NotificationOutbox()
    {
      List<NotificationOutbox> list = _mapper.GetNotificationData();
      return View(list);
    }


    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }

    public Int64 PushNotificationToApple(Notification nObj, User user)
    {
      Int64 result = 0;

      try
      {
        string strDeviceToken = user.DeviceToken;
        string strPushMessage = nObj.Message;
        var payload1 = new NotificationPayload(strDeviceToken, strPushMessage, 1, "default");
        payload1.AddCustom("RegionID", "IDQ10150");
        var p = new List<NotificationPayload> { payload1 };

        string certificatePath = Server.MapPath("/Certificates/VAZ_APNS_Development.p12");
        var push = new PushNotification(true, certificatePath, "ViewAdsZone@iOS");
        string strfilename = push.P12File;
        var message1 = push.SendToApple(p);
        foreach (var item in message1)
        {
          result = 1;
        }
      }
      catch (Exception ex)
      {
      }
      return result;
    }
  }
}