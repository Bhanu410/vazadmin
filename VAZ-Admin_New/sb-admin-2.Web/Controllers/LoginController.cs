﻿using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sb_admin_2.Web.Mappers;

namespace sb_admin_2.Web.Controllers
{
    public class LoginController : Controller
    {
        LoginMapper _mapper = new LoginMapper();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Login user)
        {
            if(_mapper.AuthenticateUser(user) != null)
            {
                Session["UserName"] = _mapper.AuthenticateUser(user).UserName;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("","Invalid Login credentials");
                return View();
            }
            
        }

        public ActionResult Logout()
        {
            Session["UserName"] = null;
            return RedirectToAction("Index");
        }

        
	}
}