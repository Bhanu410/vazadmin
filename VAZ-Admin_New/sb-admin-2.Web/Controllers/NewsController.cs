﻿using sb_admin_2.Web.Mappers;
using sb_admin_2.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace sb_admin_2.Web.Controllers
{
  public class NewsController : Controller
  {
    NewsMapper _mapper = new NewsMapper();
    OfferMapper _offerMapper = new OfferMapper();
    NewsVM VM = new NewsVM();
    LocationMapper _locationMapper = new LocationMapper();
    GlobalValuesMapper _globalsMapper = new GlobalValuesMapper();

    [SessionTimeoutAttribute]
    public ActionResult GetNews()
    {
      List<NewsDTO> NewsList = _mapper.GetNewsList();
      return View(NewsList);
    }

    [SessionTimeoutAttribute]
    [HttpGet]
    public ActionResult PostNews(long NewsId = 0)
    {
      return View(InitializeNews(NewsId));
    }

    private NewsVM InitializeNews(long NewsId)
    {
      List<SelectListItem> CategoryList = new List<SelectListItem>();
      List<SelectListItem> LocationList = new List<SelectListItem>();
      List<SelectListItem> SubLocationList = new List<SelectListItem>();

      List<Location> locations;
      List<SubLocation> subLocations;

      List<OfferCategories> categories = _offerMapper.GetCategories().ToList();

      categories.ForEach(x =>
      {
        CategoryList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.CategoryList = CategoryList;

      if (NewsId != 0)
      {
        NewsDTO NewsObj = _mapper.GetSingleNewsObj(NewsId);
        subLocations = _locationMapper.GetSubLocationsByLocation(NewsObj.LocationId == null ? 0 : NewsObj.LocationId.Value);
      }
      else
      {
        subLocations = new List<SubLocation>();
      }



      // Location dropdown

      locations = _mapper.GetNewsLocations(NewsId);

      locations.ForEach(x =>
      {
        LocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
      });

      VM.LocationList = LocationList;



      subLocations.ForEach(x =>
      {
        SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
      });

      VM.SubLocationList = SubLocationList;





      if (NewsId != 0)
      {
        VM.NewsObj = _mapper.GetSingleNewsObj(NewsId);
        VM.NewsObj.NewsImages = _mapper.GetNewsImages(NewsId);
        VM.NewsObj.PageTitle = "Edit News";
        VM.NewsObj.ButtonTitle = "Update";
      }
      else
      {
        VM.NewsObj = new NewsDTO();
        VM.NewsObj.IsHeadline = false;
        VM.NewsObj.NewsImages = new List<NewsImages>();
        VM.NewsObj.PageTitle = "Add News";
        VM.NewsObj.ButtonTitle = "Save";
      }

      return VM;
    }

    [HttpPost]
    [ValidateInput(false)]
    public ActionResult PostNews(NewsVM VM, HttpPostedFileBase[] files)
    {
      Notification nObj = new Notification();
      string ChildFolder = "News";

      if (VM.NewsObj.Id != 0)
      {
        if (files[0] == null)
        {
          if (_mapper.NewsImageCount(VM.NewsObj.Id) == 0)
          {
            ModelState.AddModelError("", "Add atleast one image");
            return View(InitializeNews(VM.NewsObj.Id));
          }
        }

        _mapper.UpdateNews(VM.NewsObj);
      }
      else
      {
        if (files[0] == null)
        {
          ModelState.AddModelError("", "Add atleast one image");
          return View(InitializeNews(VM.NewsObj.Id));
        }

        VM.NewsObj.Id = _mapper.PostNews(VM.NewsObj);

      }

      if (files[0] != null)
      {
        int count = 0;
        foreach (HttpPostedFileBase file in files)
        {
          NewsImages iObj = new NewsImages();
          iObj.NewsId = VM.NewsObj.Id;
          iObj.ImagePath = FileProcess(file, ChildFolder);

          if (count == 0)
          {
            nObj.ImagePath = iObj.ImagePath;
          }

          _mapper.UploadMultipleNewsImages(iObj);
          count++;
        }
      }


      if (VM.NewsObj.IsHeadline == true)
      {
        if (!string.IsNullOrEmpty(nObj.ImagePath))
        {
          string strApplicationId = _globalsMapper.Globals().Where(a => a.Name == "Android_PushNotification_APIKey").Select(a => a.Value).First();
          string strSenderId = _globalsMapper.Globals().Where(a => a.Name == "Android_PushNotification_SenderId").Select(a => a.Value).First();
          string strFCMUsers = _globalsMapper.Globals().Where(a => a.Name == "FCMALLUSERS").Select(a => a.Value).First();
          nObj.FCMUsers = strFCMUsers;

          nObj.Title = VM.NewsObj.Title;
          nObj.ID = VM.NewsObj.Id;
          nObj.Message = VM.NewsObj.News;
          nObj.CreatedAt = CurrentIndianDate();
          PushNotification(nObj, strApplicationId, strSenderId);
        }
      }




      return RedirectToAction("GetNews");
    }

    private string FileProcess(HttpPostedFileBase file, string ChildFolder)
    {
      string Ticks = string.Format(@"{0}_", DateTime.Now.Ticks);

      string path = Server.MapPath("~/Images/" + ChildFolder + "/");
      if (!Directory.Exists(path))
      {
        Directory.CreateDirectory(path);
      }
      string ActualPath = Path.Combine(path, Ticks + file.FileName);
      file.SaveAs(ActualPath);

      string AbsolutePath = "Images/" + ChildFolder + "/" + Ticks + file.FileName;

      return AbsolutePath;

    }

    public ActionResult DeleteNews(long NewsId)
    {
      _mapper.DeleteNews(NewsId);
      return RedirectToAction("GetNews");
    }

    public ActionResult FillSubLocationDDL(string LocationId, long NewsId = 0)
    {
      List<SelectListItem> SubLocationList = new List<SelectListItem>();
      List<long> LocationList = new List<long>();

      if (LocationId.Length > 0)
      {
        LocationId = LocationId.Trim(',');
        string[] strLocationArray = LocationId.Split(',');

        foreach (var item in strLocationArray)
        {
          LocationList.Add(long.Parse(item));
        }



        List<SubLocation> subLocations = _mapper.GetNewsSubLocations(LocationList, NewsId);

        subLocations.ForEach(x =>
        {
          SubLocationList.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = x.Flag });
        });
      }


      VM.SubLocationList = SubLocationList;
      return PartialView("_subLocationDDL", VM);
    }



    public ActionResult DeleteNewsImage(long NewsId, long ImageId)
    {
      NewsDTO NewsObj = new NewsDTO();
      List<NewsImages> NewsImages = new List<Models.NewsImages>();

      _mapper.DeleteNewsImage(ImageId);

      NewsImages = _mapper.GetNewsImages(NewsId);

      NewsObj.NewsImages = NewsImages;

      VM.NewsObj = NewsObj;
      return PartialView("_newsImages", VM);
    }

    public static void PushNotification(Notification nObj, string strApplicationId, string strSenderId)
    {
      try
      {
        string applicationID = strApplicationId;
        string senderId = strSenderId;
        string Message = "";

        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        tRequest.Method = "post";
        tRequest.ContentType = "application/json";

        Byte[] byteArray = null;

       

        string postObj = "{\"to\" : \"/topics/" + nObj.FCMUsers + "\",\"data\" : {\"message\":{\"Title\":\"" + nObj.Title + "\",\"ID\":\"" + nObj.ID + "\",\"ImagePath\":\"" + nObj.ImagePath + "\",\"Type\":\"News\",\"Message\":\"" + Message + "\",\"Date\":\"" + nObj.CreatedAt.Value.ToString("dd MMM yyyy HH:mm:ss tt") + "\"}}}";

        byteArray = Encoding.UTF8.GetBytes(postObj);



        tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
        tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
        tRequest.ContentLength = byteArray.Length;

        using (Stream dataStream = tRequest.GetRequestStream())
        {
          dataStream.Write(byteArray, 0, byteArray.Length);
          using (WebResponse tResponse = tRequest.GetResponse())
          {
            using (Stream dataStreamResponse = tResponse.GetResponseStream())
            {
              using (StreamReader tReader = new StreamReader(dataStreamResponse))
              {
                String sResponseFromServer = tReader.ReadToEnd();
                string str = sResponseFromServer;
              }
            }
          }
        }
      }
      catch
      {

      }

      
    }

    private static DateTime CurrentIndianDate()
    {
      TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
      DateTime currentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
      return currentDate;
    }
  }
}